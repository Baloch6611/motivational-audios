//
//  RecordAudioViewController.swift
//  Motivational Audios
//
//  Created by Farhan on 30/01/2021.
//  Copyright © 2021 Farhan. All rights reserved.
//

import UIKit

class RecordAudioViewController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak private var timerLbl: UILabel!
    @IBOutlet weak private var startLblsSV: UIStackView!
    @IBOutlet weak private var startBtn: UIButton!
    @IBOutlet weak private var finishBtn: UIButton!
    @IBOutlet weak private var containerView: UIView!
    @IBOutlet weak private var circularView: CircularProgressView!
    
    //MARK:- Properties
    var duration: TimeInterval!
    var recordingStatus: audioRecording = .stop

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    //MARK:- IBActions
    @IBAction private func onClickCancel(_ sender: UIButton){
        
    }
    
    @IBAction private func onClickStart(_ sender: UIButton){
        switch recordingStatus {
        case .stop: recordingStatus   = .start
        case .start: recordingStatus  = .resume
        case .resume: recordingStatus = .start
        }
        
        updateUI()
    }
    
    @IBAction private func onClickFinish(_ sender: UIButton){
        recordingStatus = .stop
        updateUI()
    }
    
    //MARK:- Custom Methods
    private func updateUI(){
        var startButtonTitle = ""
        switch recordingStatus {
        case .stop   : startButtonTitle = audioRecording.stop.rawValue.capitalized
        case .start  : startButtonTitle = audioRecording.start.rawValue.capitalized
        case .resume : startButtonTitle = audioRecording.resume.rawValue.capitalized
        }
        
        DispatchQueue.main.async {
            self.startBtn.setTitle(startButtonTitle, for: .normal)
            self.startLblsSV.subviews.forEach({ (view) in view.isHidden = self.recordingStatus == .stop ? false : true })
            self.finishBtn.isHidden = self.recordingStatus == .stop ? true : false
            self.timerLbl.isHidden = self.recordingStatus == .stop ? true : false
        }
    }

}
