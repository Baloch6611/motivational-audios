//
//  AddAudioViewController.swift
//  Motivational Audios
//
//  Created by Farhan on 15/09/2021.
//  Copyright © 2021 Farhan. All rights reserved.
//

import UIKit
import iOSDropDown
import FirebaseDatabase

class AddAudioViewController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak private var categoriesTF: DropDown!
    @IBOutlet weak private var titleLblTF: UITextField!
    
    //Recorder outlets
    @IBOutlet var playButton: UIButton!
    @IBOutlet var timeLabel: UILabel!
    
    //MARK:- Properties
    private var availableCategories = [CategoryObjectModel]()
    private var allCategoriesTitle = [String]()
    
    //Audio Recorder variables
    var recorder = AKAudioRecorder.shared
    var displayLink = CADisplayLink()
    var duration : CGFloat = 0.0
    var timer : Timer!

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        getAvailableCategories()
        setCircle()
    }
    
    //MARK:- IBActions
    @IBAction private func playstopButton(_ sender: UIButton) {
        if recorder.isRecording{
            animate(isRecording: true)
            recorder.stopRecording()
            //update view for playing recoreded audio
        } else{
            animate(isRecording: false)
            recorder.record()
            setTimer()
        }
    }
    
}
//MARK:- Get Data
extension AddAudioViewController {
    private func getAvailableCategories(){
        databaseRootReference.child("AllCategories").observe(.value, with: { (snapshot) in
            self.categoriesTF.placeholder = "Fetching available categories..."
            let categories = snapshot.children
            self.availableCategories.removeAll()
            for category in categories {
                let categoryChildData = category as! DataSnapshot
                let values = categoryChildData.value as! [String : Any]
                let obj = CategoryObjectModel()
                if let id = values["id"] as? String { obj.id = id }
                if let title = values["title"] as? String { obj.title = title }
                
                self.availableCategories.append(obj)
            }
            
            self.allCategoriesTitle = self.availableCategories.map { $0.title }
            
            if self.allCategoriesTitle.count == 0 { self.categoriesTF.placeholder = "No category found" }
            else {
                self.categoriesTF.optionArray = self.allCategoriesTitle
                self.categoriesTF.placeholder = "Select category"
                self.categoriesTF.text = self.allCategoriesTitle[0]
            }
        })
    }
}

//MARK:- Helper Methods
extension AddAudioViewController {
}
