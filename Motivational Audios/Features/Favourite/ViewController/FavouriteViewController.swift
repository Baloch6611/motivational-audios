//
//  FavouriteViewController.swift
//  Motivational Audios
//
//  Created by ADMIN on 8/15/20.
//  Copyright © 2020 Farhan. All rights reserved.
//

import UIKit

class FavouriteViewController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- Properties
    var isHidden = false
    var cellAnimation = true
    var viewModel: FavouriteViewModel?
    var favouriteAudios = [AudioObjectModel]()
    
    //MARK:- View life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setCustomFontNavBarTitle(title: "Favourites")
        viewModel = FavouriteViewModel()
        guard let viewModel = viewModel else {return}
        viewModel.delegate = self
        configureTableView()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard let viewModel = viewModel else { return }
        let userID = UserDefaults.standard.getUserID()
        if userID != "" {
            startLoading()
            viewModel.getdata()
        }
        
        else { PlayAudioViewController.showAlert(vc: self, message: "Signin to add audios to favourites.") }
    }
    
    //MARK:- Custom Methods
    private func configureTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(SearchTableViewCell.nib, forCellReuseIdentifier: SearchTableViewCell.identifier)
    }
    
    //MARK:- Alerts
    func showRemoveFromFavAlert(message: String, audio: AudioObjectModel, completion: @escaping (Bool)->()){
        let alert = UIAlertController(title: "Favourite", message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Remove", style: UIAlertAction.Style.default, handler: { action in
            let userID = UserDefaults.standard.getUserID()
            let userFavAudiosReference = databaseRootReference.child("FavAudios").child(userID)
            userFavAudiosReference.queryOrdered(byChild: "audioId").queryEqual(toValue: audio.audioId).observe(.childAdded, with: { (snapshot) in
                snapshot.ref.removeValue(completionBlock: { (error, reference) in
                    if error != nil {
                        self.showErrorMessage(withMessage: something_went_wrong)
                        completion(false)
                        return
                    }
                    completion(true)
                })
            })
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
}
//MARK:- UITableView Delegates & DataSource Implementation
extension FavouriteViewController: TableViewMethods {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return favouriteAudios.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SearchTableViewCell.identifier, for: indexPath) as! SearchTableViewCell
        cell.delegates = self
        let audio = favouriteAudios[indexPath.row]
        cell.configureCell(cell: audio)

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextVC = UIStoryboard.instantiateViewController(for: .Main, indentifier: String(describing: PlayAudioViewController.self)) as! PlayAudioViewController
        nextVC.playlist = favouriteAudios
        nextVC.hidesBottomBarWhenPushed = true
        nextVC.audioPlayingIndexPath = indexPath
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.layer.transform = CATransform3DMakeScale(0.1,0.1,1)
        UIView.animate(withDuration: 0.75, animations: {
            cell.layer.transform = CATransform3DMakeScale(1.05,1.05,1)
            },completion: { finished in
//                UIView.animate(withDuration: 0.5, animations: { cell.layer.transform = CATransform3DMakeScale(1,1,1) })
        })
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

//MARK:- Favourite View Model Delegates Implementation
extension FavouriteViewController: FavouriteViewModelDelegate {
    func onSuccess(favAudios: [AudioObjectModel]) {
        stopLoadingAnimation()
        favouriteAudios.removeAll()
        favouriteAudios = favAudios
        tableView.reloadData()
    }
    
    func onFailiure(_ errorMessage: String) {
        stopLoadingAnimation()
        showErrorMessage(withMessage: errorMessage)
    }
}

//MARK:- SearchTableViewCell Delegates Implementation
extension FavouriteViewController: SearchTableViewCellDelegates {
    func likeButtonTapped(cell: SearchTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        let audio = favouriteAudios[indexPath.row]
        self.showRemoveFromFavAlert(message: "Remove audio from your favourite list.", audio: audio) { (completion) in
            if completion == true {
                self.showToastMessage(withMessage: "Audio removed.")
                if let cell = self.tableView.cellForRow(at: indexPath) as? SearchTableViewCell {
                    if self.tableView.visibleCells.contains(cell) {
                        DispatchQueue.main.async {
                            self.cellAnimation = false
                            self.tableView.reloadRows(at: [indexPath], with: .fade)
                            DispatchQueue.global().asyncAfter(deadline: .now() + 1.5, execute: { self.cellAnimation = true })
                        }
                    }
                }
            }
        }
    }
}
