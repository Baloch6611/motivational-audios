//
//  FavouriteViewModel.swift
//  Motivational Audios
//
//  Created by Irfan Ahmed on 26/08/2020.
//  Copyright © 2020 Farhan. All rights reserved.
//

import Foundation
import UIKit
import FirebaseDatabase

protocol FavouriteViewModelDelegate {
    func onSuccess(favAudios: [AudioObjectModel])
    func onFailiure(_ errorMessage: String)
}

class FavouriteViewModel {
    
    //MARK:- Properties
    var delegate: FavouriteViewModelDelegate?
    var favAudios = [AudioObjectModel]()
    
    //MARK:- Get Data
    func getdata(){
        let userID = UserDefaults.standard.getUserID()
        databaseRootReference.child("FavAudios").child(userID).observe(.value, with: { (snapshot) in
            let moodsAndCollectionChildren = snapshot.children
            self.favAudios.removeAll()
            for moodsChild in moodsAndCollectionChildren {
                let moodsChildData = moodsChild as! DataSnapshot
                let values = moodsChildData.value as! [String : Any]
                let obj = AudioObjectModel()
                if let audio = values["audio"] as? String { obj.audio = audio }
                if let audioBy = values["audioBy"] as? String { obj.audioBy = audioBy }
                if let categoryId = values["categoryId"] as? String { obj.categoryId = categoryId }
                if let audioId = values["audioId"] as? String { obj.audioId = audioId }
                if let title = values["title"] as? String { obj.title = title }

                self.favAudios.append(obj)
            }
            
             guard let delegate = self.delegate else { return }
            delegate.onSuccess(favAudios: self.favAudios)
        })
    }

    
    
}
