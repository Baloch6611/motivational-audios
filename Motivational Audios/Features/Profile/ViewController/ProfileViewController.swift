//
//  ProfileViewControllerViewController.swift
//  Motivational Audios
//
//  Created by ADMIN on 8/15/20.
//  Copyright © 2020 Farhan. All rights reserved.
//

import UIKit
import Firebase

class ProfileViewController: UIViewController, UITextFieldDelegate {
    
    //MARK: IBOutlets
    @IBOutlet weak var profileImage: UIImageView!{didSet{profileImage.makeRounded()}}
    @IBOutlet weak var logoutBtnWidth: NSLayoutConstraint!
    @IBOutlet weak var nameTF: UITextField!
    @IBOutlet weak var emailTF: UITextField!
    @IBOutlet weak var dobTF: UITextField!
    @IBOutlet weak var gradientView: UIView!
    @IBOutlet weak var logoutBtn: UIButton!
    
    //MARK:- Properties
    var viewModel = ProfileViewModel()
    var uploadImage = false
    var userLoggedIn = false

    //MARK: View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel.delegate = self
        dobTF.delegate = self
        if UserDefaults.standard.isUserLoggedInWithEmail() { emailTF.isEnabled = false }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setGradientBackground(view: gradientView)
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let userID = UserDefaults.standard.getUserID()
        if userID != "" {
            startLoading()
            logoutBtn.setImage(Constant.shared.getImage(image: .logout), for: .normal)
            logoutBtnWidth.constant = 30
            self.view.layoutIfNeeded()
            viewModel.getProfileData(update: false)
            userLoggedIn = true
        }
        
        else {
            PlayAudioViewController.showAlert(vc: self, message: "Signin to set a profile.")
            logoutBtn.setImage(UIImage(named: ""), for: .normal)
            logoutBtn.setTitle("Signin", for: .normal)
            logoutBtnWidth.constant = 60
            self.view.layoutIfNeeded()
            userLoggedIn = false
        }
        
    }
    
    
    //MARK:- IBAction
    @IBAction func logoutBtnTapped(_ sender: UIButton){
        if !userLoggedIn { signoutUser() }
        
        let signOutAction = UIAlertAction(title: "Sign Out", style: .destructive) { [weak self] (action) in
            guard let self = self else { return }
            self.signoutUser()
        }
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        self.showAlert(on: self, style: .actionSheet, title: nil, message: nil, actions: [signOutAction, cancelAction], completion: nil)
    }
    
    @IBAction func updateBtnTapped(_ sender: UIButton) { updateProfile() }
    
    @IBAction func selectImage(_ sender: UIButton) {
        let userID = UserDefaults.standard.getUserID()
        if userID != "" {
            CameraHandler.shared.showActionSheet(vc: self)
            CameraHandler.shared.imagePickedBlock = getSelectedImage
        }
        
        else { PlayAudioViewController.showAlert(vc: self, message: "Signin to set a profile.") }
    }
    

    //MARK:- Custom Methods
    private func getSelectedImage(image: UIImage) {
        profileImage.image = image
        uploadImage = true
    }
    
    private func updateProfile(){
        let userId = UserDefaults.standard.getUserID()
        if userId == "" {
            PlayAudioViewController.showAlert(vc: self, message: "Signin to set a profile.")
            return
        }
        
        let username = nameTF.text ?? ""
        let dob = dobTF.text ?? ""
        
        guard let email = emailTF.text else { return }
        startLoading()
        viewModel.updateUserInfo(name: username, email: email, dob: dob)
        guard let image = profileImage.image else { return }
        if uploadImage { viewModel.uploadProfileImage(image: image) }
    }
    
    private func signoutUser(){
        do {
          try Auth.auth().signOut()
          UserDefaults.standard.setLoggedIn(value: false)
          UserDefaults.standard.removeObject(forKey: UserDefaultsKeys.userID.rawValue)
          let nextVC = UIStoryboard.initialViewController(for: .Registration)
          nextVC.modalPresentationStyle = .fullScreen
          self.present(nextVC, animated: true, completion: nil)
        } catch let err {
          print("Failed to sign out with error", err)
          self.showErrorMessage(withMessage: something_went_wrong)
        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField){
        if !userLoggedIn {
            textField.resignFirstResponder()
            return
        }
        
        if textField == dobTF {
            self.dobTF = textField
            
            let picker = UIDatePicker()
            picker.datePickerMode = .date
            if #available(iOS 14, *) { picker.preferredDatePickerStyle = .wheels }
            picker.addTarget(self, action: #selector(updateDOBTF(sender:)), for: .valueChanged)
            
            textField.inputView = picker
            textField.text = formatDateForDisplay(date: picker.date)
        }
    }
    
    @objc func updateDOBTF(sender: UIDatePicker) {
        dobTF.text = formatDateForDisplay(date: sender.date)
    }

    private func formatDateForDisplay(date: Date) -> String {
        let formatter = DateFormatter()
        formatter.dateFormat = "dd MMM yyyy"
        return formatter.string(from: date)
    }
    
}

//MARK:- ProfileView Model Delegate Implementation
extension ProfileViewController: ProfileViewModeDelegate {
    func onSuccess(user: UserModel, update: Bool) {
        stopLoadingAnimation()
        nameTF.text = user.username
        emailTF.text = user.email
        dobTF.text = user.dob
        if let url = URL(string: user.profileImage) {
            profileImage.kf.indicatorType = .activity
            profileImage.kf.setImage(with: url, placeholder: Constant.shared.getImage(image: .defaultUser), options: [.transition(.fade(0.5))])
        }
        
        if update { showSucessMessage(withMessage:  "Profile updated.") }
        
    }
    
    func onProfileImageUpload() { uploadImage = false }
    
    func onFailiure(_ errorMessage: String) {
        stopLoadingAnimation()
        showErrorMessage(withMessage: errorMessage)
    }
}
