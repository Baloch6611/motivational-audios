//
//  ProfileViewModel.swift
//  Motivational Audios
//
//  Created by Irfan Ahmed on 28/08/2020.
//  Copyright © 2020 Farhan. All rights reserved.
//

import Foundation
import UIKit
import FirebaseDatabase
import FirebaseStorage

protocol ProfileViewModeDelegate {
    func onSuccess(user: UserModel, update: Bool)
    func onProfileImageUpload()
    func onFailiure(_ errorMessage: String)
}

class ProfileViewModel {
    
    //MARK:- Properties
    var delegate: ProfileViewModeDelegate?
    
    //MARK:- Custom Methods
    func getProfileData(update: Bool){
        let userID = UserDefaults.standard.getUserID()
        guard userID != "" else { return }
        databaseRootReference.child("Users").child(userID).observeSingleEvent(of: .value) { (userDetail) in
            guard let values = userDetail.value as? [String : Any] else {
                guard let delegate = self.delegate else { return }
                delegate.onFailiure(something_went_wrong)
                return
            }
            
            let obj = UserModel()
            if let dob = values["dob"] as? String { obj.dob = dob }
            if let email = values["email"] as? String { obj.email = email }
            if let profileImage = values["profileImage"] as? String { obj.profileImage = profileImage }
            if let username = values["name"] as? String { obj.username = username }
            
            guard let delegate = self.delegate else { return }
            delegate.onSuccess(user: obj, update: update)
        }
    }
    
    
    func updateUserInfo(name: String, email: String, dob: String){
        
        let userValues = [
            "name" : name,
            "profileImage" : "",
            "email" : email,
            "dob" : dob
        ]
        
        let userID = UserDefaults.standard.getUserID()
        databaseRootReference.child("Users").child(userID).updateChildValues(userValues) { (error, ref) in
            guard let delegate = self.delegate else { return }
            if error != nil {
                delegate.onFailiure(something_went_wrong)
                return
            }
            
            self.getProfileData(update: true)
        }
    }
    
    
    func uploadProfileImage(image: UIImage){
        let userID = UserDefaults.standard.getUserID()
        guard let imageData = image.jpegData(compressionQuality: 0.5) else {return}
        let profileImgReference = Storage.storage().reference().child("Profile_Image_URLs").child("\(userID).png")
        let uploadTask = profileImgReference.putData(imageData, metadata: nil) { (metadata, error) in
            guard let delegate = self.delegate else { return }
            if let error = error {
                print(error.localizedDescription)
                delegate.onFailiure(something_went_wrong)
                return
            }
            else {
                profileImgReference.downloadURL { (url, error) in
                    guard let downloadURL = url else {
                        delegate.onFailiure(something_went_wrong)
                        return
                    }
                    
                    databaseRootReference.child("Users").child(userID).updateChildValues(["profileImage": "\(downloadURL)"])
                    delegate.onProfileImageUpload()
                }
            }
        }
        
        uploadTask.observe(.progress, handler: { (snapshot) in
            print(snapshot.progress?.fractionCompleted ?? "")
        })
    }
    
}
