//
//  UserModel.swift
//  Motivational Audios
//
//  Created by Irfan Ahmed on 28/08/2020.
//  Copyright © 2020 Farhan. All rights reserved.
//

import Foundation
import UIKit

class UserModel: NSObject {
    var username = ""
    var email = ""
    var dob = ""
    var profileImage = ""
    
}
