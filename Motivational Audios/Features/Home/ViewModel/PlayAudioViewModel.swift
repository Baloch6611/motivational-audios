//
//  PlayAudioViewModel.swift
//  Motivational Audios
//
//  Created by Irfan Ahmed on 28/08/2020.
//  Copyright © 2020 Farhan. All rights reserved.
//

import Foundation
import UIKit
import FirebaseDatabase

protocol PlayAudioViewModelDelegates {
    func audioAddedtoFav(indexPath: IndexPath)
    func audiosFetched(audios: [AudioObjectModel])
    func categoryImageFetched(image: String)
    func onFailiure(_ errorMessage: String)
}

class PlayAudioViewModel {
    
    //MARK:- Properties
    var delegate: PlayAudioViewModelDelegates?
    var playlist = [AudioObjectModel]()
    
    
    func addToFavList(audio: AudioObjectModel, indexPath: IndexPath) {
        let userID = UserDefaults.standard.getUserID()
        let favAudiosReference = databaseRootReference.child("FavAudios")
        let userFavAudiosReference = favAudiosReference.child(userID)
        userFavAudiosReference.queryOrdered(byChild: "audioId").queryEqual(toValue: audio.audioId)
            .observeSingleEvent(of: .value, with: { likedAudio in
                guard let delegate = self.delegate else { return }
                
                if likedAudio.exists() { delegate.onFailiure("Audio already added.") }
                    
                else {
                    let likedAudioReference = favAudiosReference.child(userID).childByAutoId()
                    
                    let audioValues = [
                        "audio"      : audio.audio,
                        "audioBy"    : audio.audioBy,
                        "categoryId" : audio.categoryId,
                        "audioId"    : audio.audioId,
                        "title"      : audio.title
                    ]
                    
                    likedAudioReference.updateChildValues(audioValues) { (error, ref) in
                        if error != nil { delegate.onFailiure(something_went_wrong) }
                        else { delegate.audioAddedtoFav(indexPath: indexPath) }
                    }
                }
            });
    }
    
    func getAudios(categoryId: String){
        databaseRootReference.child("AllAudios").child(categoryId).observe(.value, with: { (snapshot) in
            let audios = snapshot.children
            self.playlist.removeAll()
            for audio in audios {
                let audioChildData = audio as! DataSnapshot
                let values = audioChildData.value as! [String : Any]
                let obj = AudioObjectModel()
                if let audio = values["audio"] as? String { obj.audio = audio }
                if let title = values["title"] as? String { obj.title = title }
                if let audioBy = values["audioBy"] as? String { obj.audioBy = audioBy }
                if let categoryId = values["categoryId"] as? String { obj.categoryId = categoryId }
                if let audioId = values["audioId"] as? String { obj.audioId = audioId }

                self.playlist.append(obj)
            }
            
            guard let delegate = self.delegate else { return }
            delegate.audiosFetched(audios: self.playlist)
        })
    }
    
    func getCategoryCoverImage(categoryId: String){
        let ref = databaseRootReference.child("AllCategories")
        ref.queryOrdered(byChild: "id").queryEqual(toValue: categoryId).observeSingleEvent(of: DataEventType.value) { (snapshot) in
            if snapshot.exists() {
                for child in snapshot.children {
                    let childSnap = child as! DataSnapshot
                    let dict = childSnap.value as! [String: Any]
                    let cover = dict["cover"] as! String
                    guard let delegate = self.delegate else { return }
                    delegate.categoryImageFetched(image: cover)
                }
            }
        }
    }
    
}
