//
//  HomeViewModel.swift
//  Motivational Audios
//
//  Created by Irfan Ahmed on 25/08/2020.
//  Copyright © 2020 Farhan. All rights reserved.
//

import Foundation
import UIKit
import FirebaseDatabase

protocol HomeViewModelDelegate {
    func onSuccess(categories: [CategoryObjectModel])
    func onFailiure(_ errorMessage: String)
}

class HomeViewModel {
    
    //MARK:- Properties
    var delegate: HomeViewModelDelegate?
    var allCategories = [CategoryObjectModel]()
    var paginationLimit: UInt = 14
    

    //MARK:- Get Data
    func getInitialCategories(){
        databaseRootReference.child("AllCategories").queryOrderedByKey().queryLimited(toLast: paginationLimit).observe(.value, with: { (snapshot) in
            let categories = snapshot.children
            self.allCategories.removeAll()
            for category in categories {
                let categoryChildData = category as! DataSnapshot
                let values = categoryChildData.value as! [String : Any]
                let obj = CategoryObjectModel()
                if let id = values["id"] as? String { obj.id = id }
                if let title = values["title"] as? String { obj.title = title }
                if let cover = values["cover"] as? String { obj.cover = cover }

                self.allCategories.append(obj)
            }
            guard let delegate = self.delegate else { return }
            delegate.onSuccess(categories: self.allCategories)
        })
    }
    
    func getNextCategories(id lastCategoryFetched: String){
        databaseRootReference.child("AllCategories").queryOrderedByKey().queryEnding(atValue: "8").queryLimited(toLast: paginationLimit).observeSingleEvent(of: .value, with: { snapshot in
            let categories = snapshot.children
            self.allCategories.removeAll()
            for category in categories {
                let categoryChildData = category as! DataSnapshot
                let values = categoryChildData.value as! [String : Any]
                let obj = CategoryObjectModel()
                if let id = values["id"] as? String { obj.id = id }
                if let title = values["title"] as? String { obj.title = title }
                if let cover = values["cover"] as? String { obj.cover = cover }

                self.allCategories.append(obj)
            }
            guard let delegate = self.delegate else { return }
            delegate.onSuccess(categories: self.allCategories)
        })
    }

    
    
}

