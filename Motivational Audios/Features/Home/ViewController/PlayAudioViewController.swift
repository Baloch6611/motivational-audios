//
//  PlayAudioViewController.swift
//  Motivational Audios
//
//  Created by ADMIN on 8/13/20.
//  Copyright © 2020 Farhan. All rights reserved.
//

import UIKit
import FirebaseStorage
import AVFoundation
import FirebaseDatabase

class PlayAudioViewController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var thumbnailImageView: UIImageView!
    @IBOutlet weak var playButton: LoadingButton!
    @IBOutlet weak var nextButton: UIButton!
    @IBOutlet weak var prevButton: UIButton!
    @IBOutlet weak var audioTitle: UILabel!
    @IBOutlet weak var audioCurrentTime: UILabel!
    @IBOutlet weak var audioTotalDuration: UILabel!
    @IBOutlet weak var repeatButton: UIButton!
    @IBOutlet weak var muteButton: UIButton! {
        didSet { muteButton.setImageWithTintColor(image: Constant.shared.getImage(image: .soundOn), color: .white) }
    }
    
    @IBOutlet weak var backButton: UIButton! {
        didSet { backButton.setImageWithTintColor(image: Constant.shared.getImage(image: .back), color: .white) }
    }
    
    @IBOutlet weak var trackAudioSlider: UISlider! {
        didSet {
            let sliderThumbSize = CGSize(width: 15, height: 15)
            trackAudioSlider.setSliderThumbSize(sliderThumbSize)
        }
    }
    
    
    //MARK:- Properties
    var viewModel = PlayAudioViewModel()
    var audioPlayingDownloadURL = ""
    var audioPlayingIndexPath: IndexPath?
    var selectedCategory: CategoryObjectModel?
    var playlist = [AudioObjectModel]()
    var audioPlayer: AVAudioPlayer?
    
    var workItem: DispatchWorkItem!
    var backgroundQueue = DispatchQueue.global(qos: .background)
    
    var playing = false
    var audioMute = false
    var audioRepeat: AudioRepeat = .repeate
    var timer = Timer()
    var cellAnimation = true
    
    var audioPrepared: Bool = false { didSet { if audioPrepared { DispatchQueue.main.async { self.audioReadyUpdateUI() } } } }
    
    private var collectionViewWidth: CGFloat = 0
    private var indexOfCellBeforeDragging = 0
    private var animationDuration = 0.25
    
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        viewModel.delegate = self
        if let category = selectedCategory {
            playButton.showLoading()
            viewModel.getAudios(categoryId: category.id)
            viewModel.getCategoryCoverImage(categoryId: category.id)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        setGradientBackground(view: self.view)
        super.viewWillAppear(animated)
        hideNavBar()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        showNavBar()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        guard selectedCategory == nil,
            playlist.count != 0,
            let indexPath = audioPlayingIndexPath
            else { return }
        playButton.showLoading()
        let audioURL = playlist[indexPath.row].audio
        prepareAudio(downloadURL: audioURL)
    }
    
    //MARK:- IBActions
    @IBAction func backBtnTapped(_ sender: UIButton){ stopAudio() }
    
    @IBAction func playAudioBtnTapped(_ sender: UIButton){
        playing = !playing
        scheduledTimerWithTimeInterval()
        
        if playing {
            audioPlayer?.play()
            playButton.setButtonImage(image: Constant.shared.getImage(image: .pause), animationOption: .transitionCrossDissolve)
        }
            
        else {
            audioPlayer?.stop()
            playButton.setButtonImage(image: Constant.shared.getImage(image: .play), animationOption: .transitionCrossDissolve)
        }
    }
    
    @IBAction func nextBtnTapped(_ sender: UIButton) { nextAudio() }
    
    @IBAction func repeatButtonTapped(_ sender: UIButton) {
        switch audioRepeat {
        case .repeate:
            audioRepeat = AudioRepeat.shuffel
            showToastMessage(withMessage: AudioRepeat.shuffel.rawValue)
            repeatButton.setButtonImage(image: Constant.shared.getImage(image: .shuffel), animationOption: .transitionFlipFromRight)
        case .shuffel:
            audioRepeat = AudioRepeat.loop
            showToastMessage(withMessage: AudioRepeat.loop.rawValue)
            repeatButton.setButtonImage(image: Constant.shared.getImage(image: .loop), animationOption: .transitionFlipFromRight)
        case .loop:
            audioRepeat = AudioRepeat.repeate
            showToastMessage(withMessage: AudioRepeat.repeate.rawValue)
            repeatButton.setButtonImage(image: Constant.shared.getImage(image: .repeatAudio), animationOption: .transitionFlipFromRight)
        }
    }
    
    @IBAction func previousBtnTapped(_ sender: UIButton){
        guard let indexPath = audioPlayingIndexPath else { return }
        let currentAudio = indexPath.row - 1
        if currentAudio < 0  { return }
            
        else {
            playButton.showLoading()
            audioTitle.text = playlist[indexPath.row].title
            audioPlayer?.stop()
            audioPlayingIndexPath?.row -= 1
            playNewAudio()
        }
        
    }
    
    @IBAction func muteBtnTapped(_ sender: UIButton){
        audioMute = !audioMute
        
        if audioMute {
            audioPlayer?.volume = 0
            muteButton.setButtonImage(image: Constant.shared.getImage(image: .mute), animationOption: .transitionFlipFromRight)
        }
            
        else {
            audioPlayer?.volume = 3
            muteButton.setButtonImage(image: Constant.shared.getImage(image: .soundOn), animationOption: .transitionFlipFromRight)
        }
    }
    
    @IBAction func likeBtnTapped(_ sender: UIButton){
        let userID = UserDefaults.standard.getUserID()
        if userID == "" { showAlert(message: "Signin to add audios to favourite list.") }
            
        else {
            guard let indexPath = audioPlayingIndexPath else {return}
            let audio = playlist[indexPath.row]
            viewModel.addToFavList(audio: audio, indexPath: indexPath)
        }
    }
    
    
    
    //MARK:- Custom Methods
    func prepareAudio(downloadURL: String){
        if let url = URL(string: downloadURL) {
            workItem = DispatchWorkItem { [weak self] in
                guard let self = self else { return }
                
                do {
                    try AVAudioSession.sharedInstance().setCategory(.playback, mode: .default)
                    self.audioPlayer = try AVAudioPlayer(data: Data(contentsOf: url))
                    self.audioPlayer?.delegate = self
                    if !self.workItem.isCancelled { self.audioPrepared = true }
                    else { return }
                } catch let error {
                    print("error occured while audio downloading")
                    print(error.localizedDescription)
                    DispatchQueue.main.async {
                        self.stopLoadingAnimation()
                        self.showErrorMessage(withMessage: something_went_wrong)
                    }
                }
            }
            self.backgroundQueue.async(execute: workItem)
        }
    }
    
    private func playNewAudio(){
        guard let indexPath = audioPlayingIndexPath else {return}
        audioPlayingIndexPath = indexPath
        audioPlayingDownloadURL = playlist[indexPath.row].audio
        prepareAudio(downloadURL: audioPlayingDownloadURL)
    }
    
    private func nextAudio(){
        guard let indexPath = audioPlayingIndexPath else {return}
        if let prevIndexPath = audioPlayingIndexPath {
            if let prevCell = tableView.cellForRow(at: prevIndexPath) as? SearchTableViewCell {
                if tableView.visibleCells.contains(prevCell) {
                    self.cellAnimation = false
                    DispatchQueue.main.async { self.tableView.reloadRows(at: [prevIndexPath], with: .fade) }
                    DispatchQueue.global().asyncAfter(deadline: .now() + 1.5, execute: { self.cellAnimation = true })
                }
            }
        }
        
        let nextAudio = indexPath.row + 1
        if nextAudio == playlist.count {
            audioPlayingIndexPath = [0,0]
            guard let firstAudioIndex = audioPlayingIndexPath else { return }
            playButton.showLoading()
            audioTitle.text = playlist[firstAudioIndex.row].title
            audioPlayer?.stop()
            playNewAudio()
            return
        }
            
        else {
            playButton.showLoading()
            audioTitle.text = playlist[nextAudio].title
            audioPlayer?.stop()
            audioPlayingIndexPath?.row += 1
            playNewAudio()
        }
    }
    
    func stopAudio(){
        guard let workItem = workItem else {
            popVC()
            return
        }
        
        if audioPlayer != nil {
            if !workItem.isCancelled { workItem.cancel() }
            audioPlayer?.stop()
            audioPlayer = nil
        }
            
        else if !workItem.isCancelled { workItem.cancel() }
        
        popVC()
    }
    
    private func scheduledTimerWithTimeInterval(){
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(trackAudio), userInfo: nil, repeats: true)
    }
    
    private func audioReadyUpdateUI(){
        audioPlayer?.play()
        let image = Constant.shared.getImage(image: .pause)
        playButton.hideLoading(image: image)
        playing = true
        scheduledTimerWithTimeInterval()
        guard let indexPath = audioPlayingIndexPath else {return}
        UIView.animate(withDuration: 0.75) { self.audioTitle.text = self.playlist[indexPath.row].title }
        if let audioIndex = audioPlayingIndexPath {
            DispatchQueue.main.async {
                let categoryId = self.playlist[indexPath.row].categoryId
                self.viewModel.getCategoryCoverImage(categoryId: categoryId)
                if audioIndex.row + 1 == self.playlist.count {
                    self.disableButton(button: self.nextButton)
                    self.enableButton(button: self.prevButton)
                }
                    
                else if audioIndex.row == 0 {
                    self.disableButton(button: self.prevButton)
                    self.enableButton(button: self.nextButton)
                }
                    
                else {
                    self.enableButton(button: self.nextButton)
                    self.enableButton(button: self.prevButton)
                }
                
                if let cell = self.tableView.cellForRow(at: audioIndex) as? SearchTableViewCell {
                    if self.tableView.visibleCells.contains(cell) {
                        self.cellAnimation = false
                        self.tableView.reloadRows(at: [audioIndex], with: .fade)
                        DispatchQueue.global().asyncAfter(deadline: .now() + 1.5) { self.cellAnimation = true }
                    }
                }
            }
        }
    }
    
    private func enableButton(button: UIButton){
        UIView.animate(withDuration: 0.75) {
            button.alpha = 1.0
            button.isEnabled = true
        }
    }
    
    private func disableButton(button: UIButton){
        UIView.animate(withDuration: 0.75) {
            button.alpha = 0.25
            button.isEnabled = false
        }
    }
    
    private func configureTableView(){
        tableView.dataSource = self
        tableView.delegate = self
        tableView.register(SearchTableViewCell.nib, forCellReuseIdentifier: SearchTableViewCell.identifier)
    }
    
    private func resetSlider(){
        trackAudioSlider.value = 0.0
        audioTotalDuration.text = "00.00"
        audioCurrentTime.text = "00.00"
        playing = false
    }
    
    //MARK:- Objective C Methods
    @objc func trackAudio() {
        guard let audioPlayer = audioPlayer else { return }
        let normalizedTime = Float(audioPlayer.currentTime * 100.0 / audioPlayer.duration)
        
        let duration = Int((audioPlayer.duration - (audioPlayer.currentTime)))
        let minutes2 = duration/60
        let seconds2 = duration - minutes2 * 60
        audioTotalDuration.text = NSString(format: "%02d:%02d", minutes2,seconds2) as String
        
        let currentTime = Int((audioPlayer.currentTime))
        let minutes = currentTime/60
        let seconds = currentTime - minutes * 60
        audioCurrentTime.text = NSString(format: "%02d:%02d", minutes,seconds) as String
        
        trackAudioSlider.value = normalizedTime
    }
    
    //MARK:- Alerts
    class func showAlert(vc: UIViewController, message: String){
        let alert = UIAlertController(title: "Signin", message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Signin", style: UIAlertAction.Style.default, handler: { action in
            let nextVC = UIStoryboard.initialViewController(for: .Registration)
            nextVC.modalPresentationStyle = .fullScreen
            vc.present(nextVC, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        
        vc.present(alert, animated: true, completion: nil)
    }
    
    func showAudioAlert(message: String){
        let alert = UIAlertController(title: "No Data", message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Ok", style: UIAlertAction.Style.default, handler: { action in
            self.stopAudio()
            self.popVC()
        }))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAlert(message: String){
        let alert = UIAlertController(title: "Signin", message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Signin", style: UIAlertAction.Style.default, handler: { action in
            self.stopAudio()
            let nextVC = UIStoryboard.initialViewController(for: .Registration)
            nextVC.modalPresentationStyle = .fullScreen
            self.present(nextVC, animated: true, completion: nil)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAddtoFavAlert(message: String, audio: AudioObjectModel, indexPath: IndexPath){
        let alert = UIAlertController(title: "Favourite", message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Add", style: UIAlertAction.Style.default, handler: { action in
            self.viewModel.addToFavList(audio: audio, indexPath: indexPath)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showRemoveFromFavAlert(message: String, audio: AudioObjectModel, completion: @escaping (Bool)->()){
        let alert = UIAlertController(title: "Favourite", message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Remove", style: UIAlertAction.Style.default, handler: { action in
            let userID = UserDefaults.standard.getUserID()
            let userFavAudiosReference = databaseRootReference.child("FavAudios").child(userID)
            userFavAudiosReference.queryOrdered(byChild: "audioId").queryEqual(toValue: audio.audioId).observeSingleEvent(of: .childAdded, with: { (snapshot) in
                snapshot.ref.removeValue(completionBlock: { (error, reference) in
                    if error != nil {
                        self.showErrorMessage(withMessage: something_went_wrong)
                        completion(false)
                        return
                    }
                    completion(true)
                })
            })
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    
}

//MARK:- AVAudioPlayer Delegate Implementation
extension PlayAudioViewController: AVAudioPlayerDelegate {
    func audioPlayerDidFinishPlaying(_ player: AVAudioPlayer, successfully flag: Bool) {
        timer.invalidate()
        resetSlider()
        playButton.setImage(Constant.shared.getImage(image: .play), for: .normal)
        
        switch audioRepeat {
        case .repeate:
            nextAudio()
        case .shuffel:
            guard playlist.count > 1 else { return }
            let randomIndex = Int(arc4random_uniform(UInt32(playlist.count)))
            if let audioIndex = audioPlayingIndexPath {
                if let cell = tableView.cellForRow(at: audioIndex) as? SearchTableViewCell {
                    DispatchQueue.main.async { cell.nowPlayingLbl.isHidden = true   }
                }
            }
            playButton.showLoading()
            audioPlayingIndexPath = [0, randomIndex]
            audioPlayingDownloadURL = playlist[randomIndex].audio
            prepareAudio(downloadURL: audioPlayingDownloadURL)
            audioTitle.text = playlist[randomIndex].title
        case .loop:
            guard let player = audioPlayer else { return }
            playing = true
            scheduledTimerWithTimeInterval()
            playButton.setImage(Constant.shared.getImage(image: .pause), for: .normal)
            player.play()
        }
    }
}

//MARK:- TableView Delegates & DataSource Implementation
extension PlayAudioViewController: TableViewMethods {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return playlist.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SearchTableViewCell.identifier, for: indexPath) as! SearchTableViewCell
        cell.type = .playlist
        cell.delegates = self
        let audio = playlist[indexPath.row]
        cell.configureCell(cell: audio)
        if let selectedIndexPath = audioPlayingIndexPath {
            if selectedIndexPath == indexPath { DispatchQueue.main.async { cell.nowPlayingLbl.isHidden = false } }
            else { DispatchQueue.main.async { cell.nowPlayingLbl.isHidden = true } }
        }
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        if let prevIndexPath = audioPlayingIndexPath {
            if let prevCell = tableView.cellForRow(at: prevIndexPath) as? SearchTableViewCell {
                if tableView.visibleCells.contains(prevCell) {
                    DispatchQueue.main.async {
                        self.cellAnimation = false
                        tableView.reloadRows(at: [prevIndexPath], with: .fade)
                        DispatchQueue.global().asyncAfter(deadline: .now() + 1.5) { self.cellAnimation = true }
                    }
                }
            }
        }
        
        audioPlayingIndexPath = indexPath
        DispatchQueue.main.async {
            self.cellAnimation = false
            tableView.reloadRows(at: [indexPath], with: .fade)
            DispatchQueue.global().asyncAfter(deadline: .now() + 1.5) { self.cellAnimation = true }
        }
        playButton.showLoading()
        audioPlayer?.stop()
        audioPlayingIndexPath?.row = indexPath.row
        playNewAudio()
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        if cellAnimation {
            cell.layer.transform = CATransform3DMakeScale(0.1,0.1,1)
            UIView.animate(withDuration: 0.75, animations: {
                cell.layer.transform = CATransform3DMakeScale(1.05,1.05,1)
            },completion: { finished in
                //expand and compress animation
    //            UIView.animate(withDuration: 0.5, animations: { cell.layer.transform = CATransform3DMakeScale(1,1,1) })
            })
        }
        
        // Table view footer spinner for loading in pagination
//        let spinner = UIActivityIndicatorView(style: .medium)
//        spinner.startAnimating()
//        spinner.frame = CGRect(x: CGFloat(0), y: CGFloat(0), width: tableView.bounds.width, height: CGFloat(44))
//
//        self.tableView.tableFooterView = spinner
//        self.tableView.tableFooterView?.isHidden = false
        
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat { return 80 }
}

//MARK:- PlayAudioViewModel Delegates Implementation
extension PlayAudioViewController: PlayAudioViewModelDelegates {
    func audioAddedtoFav(indexPath: IndexPath) {
        showToastMessage(withMessage: "Audio added.")
        if let cell = self.tableView.cellForRow(at: indexPath) as? SearchTableViewCell {
            if self.tableView.visibleCells.contains(cell) {
                DispatchQueue.main.async {
                    self.cellAnimation = false
                    self.tableView.reloadRows(at: [indexPath], with: .fade)
                    DispatchQueue.global().asyncAfter(deadline: .now() + 1.5) { self.cellAnimation = true }
                }
            }
        }
    }
    
    func categoryImageFetched(image: String) {
        guard let url = URL(string: image) else { return }
        UIView.transition(with: thumbnailImageView,
                          duration: 0.75,
                          options: .transitionCrossDissolve,
                          animations: { self.thumbnailImageView.kf.setImage(with: url) },
                          completion: nil)
    }
    
    func audiosFetched(audios: [AudioObjectModel]) {
        stopLoadingAnimation()
        playlist = audios
        if playlist.count > 0 {
            audioPlayingIndexPath = [0,0]
            if let audio = playlist.first { prepareAudio(downloadURL: audio.audio) }
            tableView.reloadData()
        }
            
        else { showAudioAlert(message: "No audio was found.") }
    }
    
    func onFailiure(_ errorMessage: String) {
        showErrorMessage(withMessage: errorMessage)
    }
}

//MARK:- SearchTableViewCell Delegates Implementation
extension PlayAudioViewController: SearchTableViewCellDelegates {
    func likeButtonTapped(cell: SearchTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        let userId = UserDefaults.standard.getUserID()
        let audio = playlist[indexPath.row]
        if userId != "" {
            let favAudiosRef = databaseRootReference.child("FavAudios").child(userId)
            favAudiosRef.queryOrdered(byChild: "audioId").queryEqual(toValue: audio.audioId).observeSingleEvent(of: DataEventType.value) { (snapshot) in
                if snapshot.exists() {
                    self.showRemoveFromFavAlert(message: "Remove audio from your favourite list.", audio: audio) { (completion) in
                        if completion == true {
                            self.showToastMessage(withMessage: "Audio removed.")
                            if let cell = self.tableView.cellForRow(at: indexPath) as? SearchTableViewCell {
                                if self.tableView.visibleCells.contains(cell) {
                                    DispatchQueue.main.async {
                                        self.cellAnimation = false
                                        self.tableView.reloadRows(at: [indexPath], with: .fade)
                                        DispatchQueue.global().asyncAfter(deadline: .now() + 1.5) { self.cellAnimation = true }
                                    }
                                }
                            }
                        }
                    }
                }
                    
                else {
                    self.showAddtoFavAlert(message: "Add audio to your favourite list.", audio: audio, indexPath: indexPath)
                }
            }
        }
    }
}
