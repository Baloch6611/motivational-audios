//
//  SeeAllPlaylistViewController.swift
//  Motivational Audios
//
//  Created by Irfan Ahmed on 26/08/2020.
//  Copyright © 2020 Farhan. All rights reserved.
//

import UIKit

class SeeAllPlaylistViewController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    
    //MARK:- Properties
    var allAudios = [AudioObjectModel]()

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
    }
    
    //MARK:- Custom Methods
    private func configureTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(SearchTableViewCell.nib, forCellReuseIdentifier: SearchTableViewCell.identifier)
    }


}
//MARK:- UITableView Delegates & DataSource Implementation
extension SeeAllPlaylistViewController: TableViewMethods {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return allAudios.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SearchTableViewCell.identifier, for: indexPath) as! SearchTableViewCell
        let audio = allAudios[indexPath.row]
        cell.configureCell(cell: audio)

        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextVC = UIStoryboard.instantiateViewController(for: .Main, indentifier: String(describing: PlayAudioViewController.self)) as! PlayAudioViewController
        nextVC.playlist = allAudios
        nextVC.hidesBottomBarWhenPushed = true
        nextVC.audioPlayingIndexPath = indexPath
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 130
    }
}
