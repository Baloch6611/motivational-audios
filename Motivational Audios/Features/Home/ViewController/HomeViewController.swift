//
//  NewHomeViewController.swift
//  Motivational Audios
//
//  Created by Farhan on 19/09/2020.
//  Copyright © 2020 Farhan. All rights reserved.
//

import UIKit

class HomeViewController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var collectionView: UICollectionView!
    
    //MARK:- Properties
    var allCategories = [CategoryObjectModel]()
    var viewModel: HomeViewModel?
    var isPaginating = false
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setCustomFontNavBarTitle(title: "Motivational Audios")
        configureCollectionView()
        viewModel = HomeViewModel()
        guard let viewModel = viewModel else {return}
        viewModel.delegate = self
        startLoading()
        allCategories.removeAll()
        viewModel.getInitialCategories()
    }
    
    //MARK:- Custom Methods
    private func configureCollectionView(){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(HomeCollectionViewCell.nib, forCellWithReuseIdentifier: HomeCollectionViewCell.identifier)
    }
}
//MARK:- UICollectionView Delegates & DataSource Implementation
extension HomeViewController: CollectionViewMethods {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return allCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: HomeCollectionViewCell.identifier, for: indexPath) as! HomeCollectionViewCell
        let category = allCategories[indexPath.row]
        cell.configureCell(category: category)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let nextVC = UIStoryboard.instantiateViewController(for: .Main, indentifier: String(describing: PlayAudioViewController.self)) as! PlayAudioViewController
        nextVC.selectedCategory = allCategories[indexPath.row]
        nextVC.hidesBottomBarWhenPushed = true
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        cell.alpha = 0
        cell.transform = CGAffineTransform(scaleX: 0.1, y: 0.1)
        UIView.animate(withDuration: 0.5) {
          cell.alpha = 1
          cell.transform = .identity
        }
    }
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {        
        if (scrollView.contentOffset.y + 100) >= (scrollView.contentSize.height - scrollView.frame.size.height) {
            //Pagination
            if isPaginating {
                print("paginating")
                return
            }
            else {
                isPaginating = true
                print("fetch more data")
                DispatchQueue.global().asyncAfter(deadline: .now() + 10, execute: { self.isPaginating = false })
            }
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let flowLayout = collectionViewLayout as! UICollectionViewFlowLayout
        let numberofItem: CGFloat = HomeCollectionViewCell.numberOfItemsInRow
        let collectionViewWidth = self.collectionView.bounds.width
        let extraSpace = (numberofItem - 1) * flowLayout.minimumInteritemSpacing
        let inset = flowLayout.sectionInset.right + flowLayout.sectionInset.left
        let width = Int((collectionViewWidth - extraSpace - inset) / numberofItem)

        return CGSize(width: width, height: width + 50)
    }
}
//MARK:- Favourite View Model Delegates Implementation
extension HomeViewController: HomeViewModelDelegate {
    func onSuccess(categories: [CategoryObjectModel]) {
        isPaginating = false
        stopLoadingAnimation()
        allCategories += categories
        collectionView.reloadData()
    }
    
    func onFailiure(_ errorMessage: String) {
        isPaginating = false
        stopLoadingAnimation()
        showErrorMessage(withMessage: errorMessage)
    }
}
