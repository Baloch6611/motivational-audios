//
//  CategoryObjectModel.swift
//  Motivational Audios
//
//  Created by Farhan on 19/09/2020.
//  Copyright © 2020 Farhan. All rights reserved.
//

import Foundation

class CategoryObjectModel: NSObject {
    var cover = ""
    var id = ""
    var title = ""
}
