//
//  NewHomeCollectionViewCell.swift
//  Motivational Audios
//
//  Created by Farhan on 19/09/2020.
//  Copyright © 2020 Farhan. All rights reserved.
//

import UIKit

class HomeCollectionViewCell: UICollectionViewCell {
    
    //MARK:- IBOutlets
    @IBOutlet weak var thumbnail: UIImageView!
    
    //MARK:- Properties
    var isAnimated = false
    static let numberOfItemsInRow: CGFloat = 2
    static var identifier: String { return String(describing: self ) }
    static var nib: UINib { UINib(nibName: identifier, bundle: nil) }
    
    //MARK:- Awake From nib
    override func awakeFromNib() {
        super.awakeFromNib()
        self.layer.cornerRadius = 8.0
    }
    
    //MARK:- Custom Methods
    func configureCell(category: CategoryObjectModel){
        if let url = URL(string: category.cover) {
            thumbnail.kf.indicatorType = .activity
            thumbnail.kf.setImage(with: url, placeholder: Constant.shared.getImage(image: .gallery), options: [.transition(.fade(0.5))])
        }
    }
    
}
