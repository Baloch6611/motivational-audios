//
//  PlayerPlaylistCollectionViewCell.swift
//  Motivational Audios
//
//  Created by Irfan Ahmed on 01/09/2020.
//  Copyright © 2020 Farhan. All rights reserved.
//

import UIKit

class PlayerPlaylistCollectionViewCell: UICollectionViewCell {
    
    //MARK:- IBOutlets
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var audioTitle: UILabel!
    @IBOutlet weak var authorNmae: UILabel!

    //MARK:- Properties
    let height: CGFloat = 290
//    let width: CGFloat = 220
    let collectionViewScale: CGFloat = 0
    
    //MARK:- Identifier
    static var identifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        customizeUI()
    }
    
    override func prepareForReuse() {
        self.transform = CGAffineTransform(scaleX: collectionViewScale, y: collectionViewScale)
    }
    
    //MARK:- Custom Methods
    private func customizeUI(){
        self.roundCorners(cornerRadius: 12)
        self.thumbnail.roundCorners(cornerRadius: 12)
        
        self.thumbnail.layer.borderColor = UIColor.white.cgColor
        self.thumbnail.layer.borderWidth = 2
    }
    

    
    func configureCell(audio: AudioObjectModel){
        audioTitle.text = audio.title
        authorNmae.text = audio.audioBy
//        if let thumbnailURL = URL(string: audio.thumbnail) {
//            thumbnail.sd_setImage(with: thumbnailURL, placeholderImage: Constant.shared.getImage(image: .defaultThumbnail))
//        }
        self.transform = CGAffineTransform(scaleX: collectionViewScale, y: collectionViewScale)
    }

}
