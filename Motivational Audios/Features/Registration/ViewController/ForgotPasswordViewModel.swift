//
//  ForgotPasswordViewModel.swift
//  Motivational Audios
//
//  Created by ADMIN on 8/22/20.
//  Copyright © 2020 Farhan. All rights reserved.
//

import Foundation
import UIKit
import Firebase

protocol ForgotPasswordViewModelDelegate {
    func onSuccess()
    func onFailiure(_ errorMessage: String)
}

class ForgotPasswordViewModel {
    
    //MARK:- Properties
    var delegate: ForgotPasswordViewModelDelegate?
    
    func resetPassword(email: String){
        Auth.auth().sendPasswordReset(withEmail: email) { (error) in
            guard let delegate = self.delegate else { return }
            if let error = error as NSError? {
                switch AuthErrorCode(rawValue: error.code) {
                case .userNotFound?:
                    delegate.onFailiure("User not found.")
                case .invalidEmail?:
                    delegate.onFailiure("The email address is badly formatted.")
                case .invalidRecipientEmail?:
                    delegate.onFailiure("An invalid recipient email was sent in the request.")
                case .invalidSender?:
                    delegate.onFailiure("An invalid sender email is set in the console for this action.")
                case .invalidMessagePayload?:
                    delegate.onFailiure("An invalid email template for sending update email.")
                default:
                    delegate.onFailiure(something_went_wrong)
                    print("Error message: \(error.localizedDescription)")
                }
            }
                
            else {delegate.onSuccess()}
        }
    }
    
}
