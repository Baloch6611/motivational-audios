//
//  ForgotPasswordViewController.swift
//  Motivational Audios
//
//  Created by ADMIN on 8/22/20.
//  Copyright © 2020 Farhan. All rights reserved.
//

import UIKit

class ForgotPasswordViewController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var emailTF: UITextField! {didSet{emailTF.addShadow(cornerRadius: 5)}}
    @IBOutlet weak var outterView: UIView!

    //MARK:- Properties
    var viewModel: ForgotPasswordViewModel?
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = ForgotPasswordViewModel()
        viewModel?.delegate = self
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        outterView.roundCorners([.topRight, .bottomLeft], radius: 80)
    }
    
    //MARK:- IBActions
    @IBAction func backBtnTapped(_ sender: UIButton) { popVC() }
    
    @IBAction func resetBtnTapped(_ sender: UIButton) {
        guard let email = emailTF.text, !email.isEmpty else {
            showAlert(on: self, style: .alert, title: "Email!", message: "Enter your email address to reset password.")
            return
        }
        
        viewModel?.resetPassword(email: email)
    }
    
}
extension ForgotPasswordViewController: ForgotPasswordViewModelDelegate {
    func onSuccess() {
        showAlert(on: self, style: .alert, title: "Success!", message: "Check your inbox to reset your password.")
    }
    
    func onFailiure(_ errorMessage: String) {
        showErrorMessage(withMessage: errorMessage)
    }
    
}

//MARK:- UITextFields Delegates Implementations
extension ForgotPasswordViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool { textField.resignFirstResponder() }
}
