//
//  LoginViewModel.swift
//  Motivational Audios
//
//  Created by ADMIN on 8/21/20.
//  Copyright © 2020 Farhan. All rights reserved.
//

import Foundation
import UIKit
import FirebaseAuth
import FirebaseDatabase

protocol LoginViewModelDelegate {
    func onSuccess()
    func onFailiure(_ errorMessage: String)
    func onWarning(_ warningMessage: String)
}

class LoginViewModel {
   
    //MARK:- Properties
    var delegate: LoginViewModelDelegate?
    
    func signInUser(email: String, password: String){
        Auth.auth().signIn(withEmail: email, password: password) { (authResult, error) in
            guard let delegate = self.delegate else { return }
            if let error = error as NSError? {
                switch AuthErrorCode(rawValue: error.code) {
                case .userDisabled?:
                    delegate.onFailiure("The user account has been disabled by an administrator.")
                case .wrongPassword?:
                    delegate.onWarning("The password is invalid or the user does not have a password.")
                case .invalidEmail?:
                    delegate.onFailiure("The email address is malformed.")
                default:
                    print("Error: \(error.localizedDescription)")
                    delegate.onFailiure(something_went_wrong)
                }
            }
                
            else {
                guard let result = authResult else {
                    delegate.onFailiure(something_went_wrong)
                    return
                }
                
                let userid = result.user.uid
                UserDefaults.standard.setLoggedIn(value: true)
                UserDefaults.standard.setUserID(value: userid)
                UserDefaults.standard.loginWithEmail(value: true)
                
                delegate.onSuccess()
            }
        }
    }
}
