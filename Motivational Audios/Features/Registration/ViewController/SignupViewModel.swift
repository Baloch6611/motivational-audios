//
//  SignupViewModel.swift
//  Motivational Audios
//
//  Created by ADMIN on 8/19/20.
//  Copyright © 2020 Farhan. All rights reserved.
//

import Foundation
import FirebaseAuth

protocol SignupViewModelDelegate {
    func onSuccess()
    func onFailiure(_ errorMessage: String)
}

class SignupViewModel{
    
    //MARK:- Properties
    var delegate: SignupViewModelDelegate?
   
    public func CreateUser(with email: String, password: String, name: String){
        guard let delegate = delegate else { return }
        Auth.auth().createUser(withEmail: email, password: password) { (authResult, error) in
            if let error = error as NSError? {
                guard let delegate = self.delegate else { return }
                switch AuthErrorCode(rawValue: error.code){
                case .emailAlreadyInUse?:
                    delegate.onFailiure("Email already exists.")
                case .invalidEmail?:
                    delegate.onFailiure("The email address is badly formatted.")
                case .weakPassword?:
                    delegate.onFailiure("The password must be 6 characters long or more")
                default:
                    print("Error: \(error.localizedDescription)")
                    delegate.onFailiure(something_went_wrong)
                }
            }
                
            else {
                print("User signs up successfully")
                guard let authResult = authResult else {
                    delegate.onFailiure(something_went_wrong)
                    return
                }
                
                let user = authResult.user
                self.storeUserData(user: user, email: email, name: name)
            }
        }
    }
    
    public func storeUserData(user: User, email: String, name: String){
        let uid = user.uid
        
        let userRef = databaseRootReference.child("Users").child(uid)
        let values = [
            "name" : name,
            "email" : email,
            "uid" : uid,
            "profileImage" : "",
            "dob" : ""
        ]
        
        userRef.updateChildValues(values, withCompletionBlock: { (err, ref) in

            guard let delegate = self.delegate else {return}
            if err != nil {
                delegate.onFailiure(something_went_wrong)
                return
            }
            
            UserDefaults.standard.setLoggedIn(value: true)
            UserDefaults.standard.setUserID(value: uid)
            UserDefaults.standard.loginWithEmail(value: true)
            
            delegate.onSuccess()
        })
        
    }
    
}
