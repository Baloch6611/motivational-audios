//
//  SignupViewController.swift
//  Motivational Audios
//
//  Created by ADMIN on 8/12/20.
//  Copyright © 2020 Farhan. All rights reserved.
//

import UIKit

class SignupViewController: UIViewController {

    //MARK:- IBOutlets
    @IBOutlet weak var usernameTF: UITextField!{didSet{usernameTF.addShadow(cornerRadius: 5)}}
    @IBOutlet weak var emailTF: UITextField!{didSet{emailTF.addShadow(cornerRadius: 5)}}
    @IBOutlet weak var passwordTF: UITextField!{didSet{passwordTF.addShadow(cornerRadius: 5)}}
    @IBOutlet weak var confirmPasswordTF: UITextField!{didSet{confirmPasswordTF.addShadow(cornerRadius: 5)}}
    @IBOutlet weak var outterView: UIView!
    
    //MARK:- Properties
    var viewModel: SignupViewModel?
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = SignupViewModel()
        viewModel?.delegate = self
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        outterView.roundCorners([.topRight, .bottomLeft], radius: 80)
    }
    
    //MARK:- IBActions
    @IBAction func backBtnTapped(_ sender: UIButton) { popVC() }
    
    @IBAction func signupBtnTapped(_ sender: UIButton) {
        guard let viewModel = viewModel else { return }
        guard let email = emailTF.text, !email.isEmpty else {
            showAlert(on: self, style: .alert, title: "Email!", message: "Enter email to create account.")
            return
        }
        
        guard let password = passwordTF.text, !password.isEmpty else {
            showAlert(on: self, style: .alert, title: "Password!", message: "Enter password to complete signup.")
            return
        }
        
        guard let confirmPassword = confirmPasswordTF.text, !confirmPassword.isEmpty else {
            showAlert(on: self, style: .alert, title: "Password!", message: "Retype password to confirm.")
            return
        }
        
        if password != confirmPassword {
            showAlert(on: self, style: .alert, title: "Password!", message: "Password did not match.")
            return
        }
        
        let name = usernameTF.text ?? ""
        
        startLoading()
        viewModel.CreateUser(with: email, password: password, name: name)
    }
    
    @IBAction func siginBtnTapped(_ sender: UIButton) {
        let nextVC = UIStoryboard.instantiateViewController(for: .Registration, indentifier: String(describing: LoginViewController.self)) as! LoginViewController
        self.navigationController?.pushViewController(nextVC, animated: true)
    }


}
//MARK:- Signup View Model Delegates Implementation
extension SignupViewController: SignupViewModelDelegate{
    func onSuccess() {
        stopLoadingAnimation()
        showSucessMessage(withMessage: "Successfully registered user.")
        let nextVC = UIStoryboard.initialViewController(for: .Main)
        nextVC.modalPresentationStyle = .fullScreen
        self.present(nextVC, animated: true, completion: nil)
    }
    
    func onFailiure(_ errorMessage: String) {
        stopLoadingAnimation()
        if errorMessage == "Email already exists." {
            self.showAlert(on: self, style: .alert, title: "Error!", message: errorMessage)
        }
        else { showErrorMessage(withMessage: errorMessage) }
    }
}

//MARK:- UITextFields Delegates Implementations
extension SignupViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case usernameTF:
            usernameTF.resignFirstResponder()
            emailTF.becomeFirstResponder()
        case emailTF:
            emailTF.resignFirstResponder()
            passwordTF.becomeFirstResponder()
        case passwordTF:
            passwordTF.resignFirstResponder()
            confirmPasswordTF.becomeFirstResponder()
        case passwordTF:
            confirmPasswordTF.resignFirstResponder()
        default:
            return false
        }
        return false
    }
}

