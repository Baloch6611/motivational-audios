//
//  MainLoginViewController.swift
//  Motivational Audios
//
//  Created by ADMIN on 8/12/20.
//  Copyright © 2020 Farhan. All rights reserved.
//

import UIKit
import TwitterKit
import FBSDKLoginKit
import AuthenticationServices
import FirebaseAuth
import FirebaseFunctions
import FirebaseStorage
import CryptoKit

class MainLoginViewController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var signInWithAppleContainerView: UIView!
    
    //MARK:- Properties
    fileprivate var currentNonce: String?
    var twitterSession: TWTRSession?
    var name = ""
    var appleLoggedIn = false
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupAppleLoginButton()
    }
    
    //MARK:- IBActions
    @IBAction func siginBtnTapped(_ sender: UIButton) {
        let nextVC = UIStoryboard.instantiateViewController(for: .Registration, indentifier: String(describing: LoginViewController.self)) as! LoginViewController
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    @IBAction func sigupBtnTapped(_ sender: UIButton) {
        let nextVC = UIStoryboard.instantiateViewController(for: .Registration, indentifier: String(describing: SignupViewController.self)) as! SignupViewController
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    @IBAction func skipBtnTapped(_ sender: UIButton) {
        let nextVC = UIStoryboard.initialViewController(for: .Main)
        UserDefaults.standard.setLoggedIn(value: true)
        nextVC.modalPresentationStyle = .fullScreen
        self.present(nextVC, animated: true, completion: nil)
    }
    
    @IBAction func loginWithFBBtnTapped(_ sender: UIButton) { loginWithFacebook() }
    
    @IBAction func loginWithTwitterBtnTapped(_ sender: UIButton) { loginWithTwitter() }
    
    
    //MARK:- Sign in With Apple
    private func setupAppleLoginButton(){
        let signInWithAppleButton = ASAuthorizationAppleIDButton()
        signInWithAppleButton.frame = signInWithAppleContainerView.bounds
        signInWithAppleButton.addTarget(self, action: #selector(signInWithApplePressed), for: .touchUpInside)
        signInWithAppleContainerView.addSubview(signInWithAppleButton)
    }
    
    @objc func signInWithApplePressed() { performAppleSignIn() }
    
    private func performAppleSignIn(){
        let request = createAppleIdRequest()
        let authorizationController = ASAuthorizationController(authorizationRequests: [request])
        authorizationController.delegate = self
        authorizationController.presentationContextProvider = self
        authorizationController.performRequests()
    }
    
    private func createAppleIdRequest()-> ASAuthorizationAppleIDRequest {
        let appleIdProvider = ASAuthorizationAppleIDProvider()
        let request = appleIdProvider.createRequest()
        request.requestedScopes = [.fullName, .email]
        
        let nonce = randomNonceString()
        request.nonce = sha256(nonce)
        currentNonce = nonce
        return request
    }
    
    private func randomNonceString(length: Int = 32) -> String {
        precondition(length > 0)
        let charset: Array<Character> =
            Array("0123456789ABCDEFGHIJKLMNOPQRSTUVXYZabcdefghijklmnopqrstuvwxyz-._")
        var result = ""
        var remainingLength = length
        
        while remainingLength > 0 {
            let randoms: [UInt8] = (0 ..< 16).map { _ in
                var random: UInt8 = 0
                let errorCode = SecRandomCopyBytes(kSecRandomDefault, 1, &random)
                if errorCode != errSecSuccess {
                    fatalError("Unable to generate nonce. SecRandomCopyBytes failed with OSStatus \(errorCode)")
                }
                return random
            }
            
            randoms.forEach { random in
                if remainingLength == 0 {
                    return
                }
                
                if random < charset.count {
                    result.append(charset[Int(random)])
                    remainingLength -= 1
                }
            }
        }
        
        return result
    }
    
    private func sha256(_ input: String) -> String {
        let inputData = Data(input.utf8)
        let hashedData = SHA256.hash(data: inputData)
        let hashString = hashedData.compactMap {
            return String(format: "%02x", $0)
        }.joined()
        
        return hashString
    }
    
    
    //MARK:- Login with Twitter
    private func loginWithTwitter(){
        startLoading()
        TWTRTwitter.sharedInstance().logIn { (session, err) in
            if let err = err {
                print("Failed to log in with Twitter with error:", err)
                self.stopLoadingAnimation()
                self.showErrorMessage(withMessage: something_went_wrong)
                return
            }
            
            guard let session = session else {
                self.stopLoadingAnimation()
                self.showErrorMessage(withMessage: something_went_wrong)
                return
            }
            
            self.twitterSession = session
            
            self.createTwitterLoginCredential { [weak self] (credentail) in
                guard let self = self else { return }
                guard let credential = credentail else { return }
                self.signIntoFirebase(credential: credential)
            }
        }
    }
    
    private func createTwitterLoginCredential(completion: @escaping(AuthCredential?)->()){
        guard let twitterSession = twitterSession else {
            self.stopLoadingAnimation()
            self.showErrorMessage(withMessage: something_went_wrong)
            completion(nil)
            return
        }
        
        let credential = TwitterAuthProvider.credential(withToken: twitterSession.authToken, secret: twitterSession.authTokenSecret)
        
        completion(credential)
    }
    
    private func fetchTwitterUser(completion: @escaping ()->()) {
        guard let twitterSession = twitterSession else {
            self.stopLoadingAnimation()
            self.showErrorMessage(withMessage: something_went_wrong)
            completion()
            return
        }
        let client = TWTRAPIClient()
        client.loadUser(withID: twitterSession.userID, completion: { (user, err) in
            if err != nil {
                self.stopLoadingAnimation()
                self.showErrorMessage(withMessage: "Twitter user error")
                completion()
                return
            }
            
            guard let user = user else {
                self.stopLoadingAnimation()
                self.showErrorMessage(withMessage: something_went_wrong)
                completion()
                return
            }
            self.name = user.name
            completion()
        })
    }
    
    //MARK:- Login with Facebook
    private func loginWithFacebook(){
        startLoading()
        let loginManager = LoginManager()
        loginManager.logIn(permissions: [.publicProfile, .email], viewController: self) { (result) in
            switch result {
            case .success(granted: _, declined: _, token: _):
                self.createFacebookLoginCredential { [weak self] (credential) in
                    guard let self = self else { return }
                    guard let credential = credential else {
                        self.stopLoadingAnimation()
                        return
                    }
                    self.signIntoFirebase(credential: credential)
                }
            case .failed:
                self.stopLoadingAnimation()
                self.showErrorMessage(withMessage: something_went_wrong)
            case .cancelled:
                self.stopLoadingAnimation()
            }
        }
    }
    
    private func createFacebookLoginCredential(completion: @escaping(AuthCredential?)->()) {
        guard let authenticationToken = AccessToken.current?.tokenString else {
            completion(nil)
            return
        }
        
        let credential = FacebookAuthProvider.credential(withAccessToken: authenticationToken)
        
        completion(credential)
    }
    
    //MARK:- Firebase Auth
    private func signIntoFirebase(credential: AuthCredential) {
        Auth.auth().signIn(with: credential) { (user, err) in
            if err != nil {
                self.stopLoadingAnimation()
                if err?.localizedDescription == email_already_exists {
                    self.showAlert(on: self, style: .alert, title: "Error!", message: "Email already exists.")
                }
                else {
                    self.showErrorMessage(withMessage: something_went_wrong)
                }

                return
            }
            print("Succesfully authenticated with Firebase.")
            self.saveUserIntoFirebaseDatabase()
        }
    }
    
    //MARK:- Firebase Database
    private func saveUserIntoFirebaseDatabase() {
        guard let uid = Auth.auth().currentUser?.uid
        else {
            self.stopLoadingAnimation()
            self.showErrorMessage(withMessage: something_went_wrong)
            return
        }
        
        guard let email = Auth.auth().currentUser?.email else { return }
        
        getUserProfileImage(uid: uid) { [weak self](profileImageDownloadURL) in
            guard let self = self else { return }
            let userRef = databaseRootReference.child("Users").child(uid)
            let values = [
                "name" : self.name,
                "email" : email,
                "uid" : uid,
                "profileImage" : profileImageDownloadURL,
                "dob" : ""
            ]
            
            self.appleLoggedIn = true
            userRef.updateChildValues(values, withCompletionBlock: { (err, ref) in
                if err != nil {
                    self.stopLoadingAnimation()
                    self.showErrorMessage(withMessage: something_went_wrong)
                    return
                }
                
                UserDefaults.standard.setLoggedIn(value: true)
                UserDefaults.standard.setUserID(value: uid)
                let nextVC = UIStoryboard.initialViewController(for: .Main)
                nextVC.modalPresentationStyle = .fullScreen
                self.present(nextVC, animated: true, completion: nil)
                
                self.showSucessMessage(withMessage: "Successfully logged in.")
            })
        }
    }
    
    private func getUserProfileImage(uid: String,completion: @escaping((String)->Void)) {
        let profileImgReference = Storage.storage().reference().child("Profile_Image_URLs").child("\(uid).png")
        profileImgReference.downloadURL { (url, error) in
            if error != nil { completion("") }
            if let downloadURL = url { completion("\(downloadURL)") }
        }
    }
}
//MARK:- ASAuthorizationControllerDelegate Implementation
extension MainLoginViewController: ASAuthorizationControllerDelegate {
    func authorizationController(controller: ASAuthorizationController, didCompleteWithAuthorization authorization: ASAuthorization) {
        if let appleIDCredential = authorization.credential as? ASAuthorizationAppleIDCredential {
            guard let nonce = currentNonce else {
                self.showErrorMessage(withMessage: something_went_wrong)
                fatalError("Invalid state: A login callback was received, but no login request was sent.")
            }
            guard let appleIDToken = appleIDCredential.identityToken else {
                print("Unable to fetch identity token")
                self.showErrorMessage(withMessage: something_went_wrong)
                return
            }
            guard let idTokenString = String(data: appleIDToken, encoding: .utf8) else {
                print("Unable to serialize token string from data: \(appleIDToken.debugDescription)")
                self.showErrorMessage(withMessage: something_went_wrong)
                return
            }
            
            let credential = OAuthProvider.credential(withProviderID: "apple.com",
                                                      idToken: idTokenString,
                                                      rawNonce: nonce)
            
            signInAppleUserWithFirebase(credential: credential)
            
//            if let email = appleIDCredential.email {
//                checkEmailAvailability(email: email) { (emailAvailable) in
//                    if emailAvailable { self.signInAppleUserWithFirebase(credential: credential) }
//                    else {
//                        self.showAlert(on: self, style: .alert, title: "Error!", message: "Email already exists.")
//                    }
//                }
//            }
//            else { signInAppleUserWithFirebase(credential: credential) }
        }
    }
    
    func checkEmailAvailability(email: String, completion:@escaping((Bool)->Void)) {
        let databaseRefence = databaseRootReference.child("Users")

        databaseRefence.queryOrdered(byChild: "email").queryStarting(atValue: email).queryEnding(atValue: email+"\u{f8ff}").observeSingleEvent(of: .value, with: { snapshot in
            if snapshot.exists() {
                if self.appleLoggedIn { return }
                completion(false)
            }
            
            else { completion(true) }
        })
    }
    
    func signInAppleUserWithFirebase(credential: OAuthCredential){
        Auth.auth().signIn(with: credential) { (authResult, error) in
            if (error != nil) {
                self.showErrorMessage(withMessage: something_went_wrong)
                print(error!.localizedDescription)
                return
            }
            self.saveUserIntoFirebaseDatabase()
        }
    }
    
    func authorizationController(controller: ASAuthorizationController, didCompleteWithError error: Error) {
        showErrorMessage(withMessage: something_went_wrong)
        print("Sign in with Apple errored: \(error)")
    }
    
}

extension MainLoginViewController: ASAuthorizationControllerPresentationContextProviding {
    func presentationAnchor(for controller: ASAuthorizationController) -> ASPresentationAnchor {
        return self.view.window!
    }
}
