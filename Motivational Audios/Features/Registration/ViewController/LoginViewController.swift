//
//  LoginViewController.swift
//  Motivational Audios
//
//  Created by ADMIN on 8/12/20.
//  Copyright © 2020 Farhan. All rights reserved.
//

import UIKit

class LoginViewController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var emailTF: UITextField!{didSet{emailTF.addShadow(cornerRadius: 5)}}
    @IBOutlet weak var passwordTF: UITextField!{didSet{passwordTF.addShadow(cornerRadius: 5)}}
    @IBOutlet weak var outterView: UIView!
    
    //MARK:- Properties
    var viewModel: LoginViewModel?
    
    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = LoginViewModel()
        viewModel?.delegate = self
    }
    
    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        outterView.roundCorners([.topRight, .bottomLeft], radius: 80)
    }

    //MARK:- IBActions
    @IBAction func backBtnTapped(_ sender: UIButton) { popVC() }
    
    @IBAction func skipBtnTapped(_ sender: UIButton) {
        let nextVC = UIStoryboard.initialViewController(for: .Main)
        UserDefaults.standard.setLoggedIn(value: true)
        nextVC.modalPresentationStyle = .fullScreen
        self.present(nextVC, animated: true, completion: nil)
    }

    
    @IBAction func signinBtnTapped(_ sender: UIButton) {
        guard let viewModel = viewModel else { return }
        guard let email = emailTF.text, !email.isEmpty else {
            showAlert(on: self, style: .alert, title: "Email!", message: "Enter email to login.")
            return
        }
        
        guard let password = passwordTF.text, !password.isEmpty else {
            showAlert(on: self, style: .alert, title: "Password!", message: "Enter password to complete login.")
            return
        }
        
        startLoading()
        viewModel.signInUser(email: email, password: password)
    }
    
    @IBAction func forgotPasswordBtnTapped(_ sender: UIButton) {
        let nextVC = UIStoryboard.instantiateViewController(for: .Registration, indentifier: String(describing: ForgotPasswordViewController.self)) as! ForgotPasswordViewController
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
        
    @IBAction func sigupBtnTapped(_ sender: UIButton) {
        let nextVC = UIStoryboard.instantiateViewController(for: .Registration, indentifier: String(describing: SignupViewController.self)) as! SignupViewController
        self.navigationController?.pushViewController(nextVC, animated: true)
    }

    
}
//MARK:- Login View Model Delegates Implementation
extension LoginViewController: LoginViewModelDelegate {
    func onSuccess() {
        stopLoadingAnimation()
        showSucessMessage(withMessage: "Successfully logged in.")
        let nextVC = UIStoryboard.initialViewController(for: .Main)
        nextVC.modalPresentationStyle = .fullScreen
        self.present(nextVC, animated: true, completion: nil)
    }
    
    func onFailiure(_ errorMessage: String) {
        stopLoadingAnimation()
        showErrorMessage(withMessage: errorMessage)
    }
    
    func onWarning(_ warningMessage: String) {
        stopLoadingAnimation()
        showWarningMessage(withMessage: warningMessage)
    }
}

//MARK:- UITextFields Delegates Implementations
extension LoginViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        switch textField {
        case emailTF:
            resignFirstResponder()
            passwordTF.becomeFirstResponder()
        case passwordTF:
            passwordTF.resignFirstResponder()
        default:
            return false
        }
        return false
    }
}
