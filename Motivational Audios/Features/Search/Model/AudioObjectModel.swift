//
//  AudioObjectModel.swift
//  Motivational Audios
//
//  Created by Irfan Ahmed on 25/08/2020.
//  Copyright © 2020 Farhan. All rights reserved.
//

import Foundation

class AudioObjectModel: NSObject {
    var audio = ""
    var audioBy = ""
    var categoryId = ""
    var audioId = ""
    var title = ""
}
