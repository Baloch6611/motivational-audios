//
//  SearchTableViewCell.swift
//  Motivational Audios
//
//  Created by ADMIN on 8/13/20.
//  Copyright © 2020 Farhan. All rights reserved.
//

import UIKit
import FirebaseDatabase
import Kingfisher

protocol SearchTableViewCellDelegates {
    func likeButtonTapped(cell: SearchTableViewCell)
}

class SearchTableViewCell: UITableViewCell {
    
    //MARK:- IBOutlets
    @IBOutlet weak var thumbnail: UIImageView!
    @IBOutlet weak var titleLbl: UILabel!
    @IBOutlet weak var authorLbl: UILabel!
    @IBOutlet weak var deviderSV: UIStackView!
    @IBOutlet weak var nowPlayingLbl: UILabel!
    @IBOutlet weak var likeBtn: UIButton!
    
    //MARK:- Properties
    var delegates: SearchTableViewCellDelegates?
    var type: SearchTableViewCellType = .search
    
    //MARK:- Identifier
    static var identifier: String { return String(describing: self) }
    
    static var nib: UINib { return UINib(nibName: identifier, bundle: nil) }
    
    //MARK:- Awake from nib
    override func awakeFromNib() {
        super.awakeFromNib()
        DispatchQueue.main.async {
            if self.type == .playlist { for view in self.deviderSV.subviews { view.isHidden = true } }
            self.nowPlayingLbl.isHidden = true
        }
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    //MARK:- IBActions
    @IBAction func likeBtnTapped(_ sender: UIButton){
        guard let delegate = delegates else { return }
        delegate.likeButtonTapped(cell: self)
    }
    
    //MARK:- Custom Methods
    func configureCell(cell: AudioObjectModel){
        titleLbl.text = cell.title.capitalized
//        authorLbl.text = cell.audioBy.capitalized
        let allCategoriesRef = databaseRootReference.child("AllCategories")
        allCategoriesRef.queryOrdered(byChild: "id").queryEqual(toValue: cell.categoryId).observeSingleEvent(of: DataEventType.value) { (snapshot) in
            if snapshot.exists() {
                for child in snapshot.children {
                    let childSnap = child as! DataSnapshot
                    let dict = childSnap.value as! [String: Any]
                    let cover = dict["cover"] as! String
                    guard let thumbnailURL  = URL(string: cover) else { return }
                    self.thumbnail.kf.indicatorType = .activity
                    self.thumbnail.kf.setImage(with: thumbnailURL, placeholder: Constant.shared.getImage(image: .gallery), options: [.transition(.fade(0.5))])
                }
            }
        }
        
        let userId = UserDefaults.standard.getUserID()
        if userId != "" {
            let favAudiosRef = databaseRootReference.child("FavAudios").child(userId)
            favAudiosRef.queryOrdered(byChild: "audioId").queryEqual(toValue: cell.audioId).observeSingleEvent(of: DataEventType.value) { (snapshot) in
                if snapshot.exists() { self.likeBtn.setImage(Constant.shared.getImage(image: .liked), for: .normal) }
                else { self.likeBtn.setImage(Constant.shared.getImage(image: .like), for: .normal) }
            }
        }
    }
    
}
