//
//  CategoryTitleCollectionViewCell.swift
//  Motivational Audios
//
//  Created by Farhan on 22/09/2020.
//  Copyright © 2020 Farhan. All rights reserved.
//

import UIKit

class CategoryTitleCollectionViewCell: UICollectionViewCell {
    
    //MARK:- IBOutlet
    @IBOutlet weak var outterView: CustomView!
    @IBOutlet weak var titleLbl: UILabel!

    //MARK:- Identifier
    static var identifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: identifier, bundle: nil)
    }
    
    //MARK:- Awake from nib
    override func awakeFromNib() {
    }

}
