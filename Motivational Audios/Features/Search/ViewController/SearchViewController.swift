//
//  SearchViewController.swift
//  Motivational Audios
//
//  Created by ADMIN on 8/13/20.
//  Copyright © 2020 Farhan. All rights reserved.
//

import UIKit
import FirebaseDatabase

class SearchViewController: UIViewController {
    
    //MARK:- IBOutlets
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var searchTF: UITextField! { didSet { searchTF.returnKeyType = UIReturnKeyType.search } }
    
    //MARK:- Properties
    var isHidden = false
    var cellAnimation = true
    var viewModel: SearchViewModel?
    var favViewModel: PlayAudioViewModel?
    var allAudios = [AudioObjectModel]()
    var filteredAudios = [AudioObjectModel]()
    var allCategories = [CategoryObjectModel]()
    var selectedCategories = [String]()
    
    //MARK:- View life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        configureTableView()
        configureCollectionView()
        viewModel = SearchViewModel()
        guard let viewModel = viewModel else { return }
        viewModel.delegate = self
        favViewModel = PlayAudioViewModel()
        guard let favViewModel = favViewModel else { return }
        favViewModel.delegate = self
        searchTF.delegate = self
        viewModel.getCategories()
    }
    
    //MARK:- IBActions
    @IBAction func searchButtonTapped(_ sender: UIButton){ }
    
    //MARK:- Custom Methods
    private func configureTableView(){
        tableView.delegate = self
        tableView.dataSource = self
        tableView.register(SearchTableViewCell.nib, forCellReuseIdentifier: SearchTableViewCell.identifier)
    }
    
    private func configureCollectionView(){
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.register(CategoryTitleCollectionViewCell.nib, forCellWithReuseIdentifier: CategoryTitleCollectionViewCell.identifier)
    }
    
    func estimatedFrame(text: String, font: UIFont) -> CGRect {
    let size = CGSize(width: 200, height: 40)
    let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
    return NSString(string: text).boundingRect(with: size,
                                               options: options,
                                               attributes: [NSAttributedString.Key.font: font],
                                               context: nil)
    }
    
    private func searchAudios(categoryid: String){
        guard let viewModel = viewModel else { return }
        viewModel.getCategoriesAudios(categoryid: categoryid)
    }
    
    private func updateSelectedCategories(indexPath: IndexPath) {
        let selectedCategoryId = allCategories[indexPath.row].id
        if selectedCategories.contains(selectedCategoryId) {
            if selectedCategories.count == 1 { return }
            if let index = selectedCategories.firstIndex(of: selectedCategoryId) { selectedCategories.remove(at: index) }
            DispatchQueue.main.async { UIView.animate(withDuration: 1.0) { self.collectionView.reloadItems(at: [indexPath]) } }
            for audio in allAudios {
                if audio.categoryId == selectedCategoryId {
                    if let index = allAudios.firstIndex(of: audio) { allAudios.remove(at: index) }
                    if let index = filteredAudios.firstIndex(of: audio) { filteredAudios.remove(at: index) }
                }
            }
            
            DispatchQueue.main.async { self.tableView.reloadData() }
        }
            
        else {
            selectedCategories.append(selectedCategoryId)
            DispatchQueue.main.async { UIView.animate(withDuration: 1.0) { self.collectionView.reloadItems(at: [indexPath]) } }
            searchAudios(categoryid: selectedCategoryId)
        }
    }
    
    //MARK:- Alerts
    func showRemoveFromFavAlert(message: String, audio: AudioObjectModel, completion: @escaping (Bool)->()){
        let alert = UIAlertController(title: "Favourite", message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Remove", style: UIAlertAction.Style.default, handler: { action in
            let userID = UserDefaults.standard.getUserID()
            let userFavAudiosReference = databaseRootReference.child("FavAudios").child(userID)
            userFavAudiosReference.queryOrdered(byChild: "audioId").queryEqual(toValue: audio.audioId).observe(.childAdded, with: { (snapshot) in
                snapshot.ref.removeValue(completionBlock: { (error, reference) in
                    if error != nil {
                        self.showErrorMessage(withMessage: something_went_wrong)
                        completion(false)
                        return
                    }
                    completion(true)
                })
            })
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }
    
    func showAddtoFavAlert(message: String, audio: AudioObjectModel, indexPath: IndexPath){
        let alert = UIAlertController(title: "Favourite", message: message, preferredStyle: UIAlertController.Style.alert)
        
        alert.addAction(UIAlertAction(title: "Add", style: UIAlertAction.Style.default, handler: { action in
            guard let favViewModel = self.favViewModel else { return }
             favViewModel.addToFavList(audio: audio, indexPath: indexPath)
        }))
        
        alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertAction.Style.cancel, handler: nil))
        
        self.present(alert, animated: true, completion: nil)
    }

}
//MARK:- UITableView Delegates & DataSource Implementation
extension SearchViewController: TableViewMethods {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if filteredAudios.count == 0 { tableView.setEmptyView(message: "No audio found.") }
        else { tableView.restore() }
        return filteredAudios.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: SearchTableViewCell.identifier, for: indexPath) as! SearchTableViewCell
        cell.type = .playlist
        cell.delegates = self
        let audio = filteredAudios[indexPath.row]
        cell.configureCell(cell: audio)
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let nextVC = UIStoryboard.instantiateViewController(for: .Main, indentifier: String(describing: PlayAudioViewController.self)) as! PlayAudioViewController
        nextVC.playlist = filteredAudios
        nextVC.hidesBottomBarWhenPushed = true
        nextVC.audioPlayingIndexPath = indexPath
        self.navigationController?.pushViewController(nextVC, animated: true)
    }
    
    func tableView(_ tableView: UITableView, willDisplay cell: UITableViewCell, forRowAt indexPath: IndexPath) {
        cell.layer.transform = CATransform3DMakeScale(0.1,0.1,1)
        UIView.animate(withDuration: 0.75, animations: {
            cell.layer.transform = CATransform3DMakeScale(1.05,1.05,1)
            },completion: { finished in
//                UIView.animate(withDuration: 0.5, animations: { cell.layer.transform = CATransform3DMakeScale(1,1,1) })
        })
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 70
    }
}

//MARK:- UICollectionView Delegates & DataSource
extension SearchViewController: CollectionViewMethods {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let emptyLabelFrame = CGRect(origin: CGPoint(x: 0,y :0), size: CGSize(width: self.view.bounds.size.width, height: self.view.bounds.size.height))
        let messageLabel = UILabel(frame: emptyLabelFrame)
        messageLabel.text = "No audios found"
        messageLabel.textColor = .white
        messageLabel.numberOfLines = 0
        messageLabel.textAlignment = .center
        messageLabel.font = UIFont(name: Constant.shared.getFontName(font: .medium), size: 14)
        messageLabel.sizeToFit()
        
        messageLabel.isHidden = allCategories.count == 0 ? false : true
        tableView.backgroundView = messageLabel
        
        return allCategories.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: CategoryTitleCollectionViewCell.identifier, for: indexPath) as! CategoryTitleCollectionViewCell
        let category = allCategories[indexPath.row]
        cell.titleLbl.text = category.title
        if selectedCategories.contains(category.id) {
            DispatchQueue.main.async {
                cell.outterView.backgroundColor = .white
                cell.titleLbl.textColor = .black
            }
        }
        else {
            DispatchQueue.main.async {
                cell.outterView.backgroundColor = .clear
                cell.titleLbl.textColor = .white
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        updateSelectedCategories(indexPath: indexPath)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let categoryTitle = allCategories[indexPath.row].title
        let width = self.estimatedFrame(text: categoryTitle, font: UIFont(name: Constant.shared.getFontName(font: .semi_bold), size: 15) ?? UIFont.systemFont(ofSize: 20)).width + 20
    
        return CGSize(width: width, height: 30.0)
    }
}

//MARK:- UITextfield delegates Implementation
extension SearchViewController: UITextFieldDelegate {
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == searchTF { searchTF.resignFirstResponder() }
        
        return true
    }
    
    func textFieldDidChangeSelection(_ textField: UITextField) {
        if textField == searchTF {
            guard let text = textField.text else { return }
            filteredAudios.removeAll()
            if text.count != 0 {
                for dicData in allAudios {
                    let isMachingModel : String = (dicData.title) as String
                    let range = isMachingModel.lowercased().range(of: text, options: String.CompareOptions.caseInsensitive, range: nil,   locale: nil)
                    if range != nil { filteredAudios.append(dicData) }
                }
            }
            else { filteredAudios = allAudios }
            self.tableView.reloadData()
        }
    }
}

//MARK: Home View Model Delegates Implementation
extension SearchViewController: SearchViewModelDelegate {
    func categoriesFetched(categories: [CategoryObjectModel]) {
        self.allCategories.removeAll()
        allCategories = categories
        if let selectedCategory = allCategories.first {
            selectedCategories.append(selectedCategory.id)
            searchAudios(categoryid: selectedCategory.id)
        }
        
        self.collectionView.reloadData()
    }
    
    func onSuccess(searchedAudio: [AudioObjectModel]) {
        stopLoadingAnimation()
        if searchedAudio.count > 0 {
            self.filteredAudios.removeAll()
            allAudios += searchedAudio
            filteredAudios = allAudios
            filteredAudios.reverse()
            tableView.reloadData()
        }
        
        else if filteredAudios.count == 0 { tableView.reloadData() }
    }
    
    func onSearchFailiure(_ errorMessage: String) {
        stopLoadingAnimation()
        self.allAudios.removeAll()
        self.filteredAudios.removeAll()
        self.tableView.reloadData()
    }
}

//MARK:- SearchTableViewCell Delegates Implementation
extension SearchViewController: SearchTableViewCellDelegates {
    func likeButtonTapped(cell: SearchTableViewCell) {
        guard let indexPath = tableView.indexPath(for: cell) else { return }
        let userId = UserDefaults.standard.getUserID()
        let audio = filteredAudios[indexPath.row]
        if userId != "" {
            let favAudiosRef = databaseRootReference.child("FavAudios").child(userId)
            favAudiosRef.queryOrdered(byChild: "audioId").queryEqual(toValue: audio.audioId).observeSingleEvent(of: DataEventType.value) { (snapshot) in
                if snapshot.exists() {
                    self.showRemoveFromFavAlert(message: "Remove audio from your favourite list.", audio: audio) { (completion) in
                        if completion == true {
                            self.showToastMessage(withMessage: "Audio removed.")
                            if let cell = self.tableView.cellForRow(at: indexPath) as? SearchTableViewCell {
                                if self.tableView.visibleCells.contains(cell) {
                                    DispatchQueue.main.async {
                                        self.cellAnimation = false
                                        self.tableView.reloadRows(at: [indexPath], with: .fade)
                                        DispatchQueue.global().asyncAfter(deadline: .now() + 1.5, execute: { self.cellAnimation = true })
                                    }
                                }
                            }
                        }
                    }
                }
                    
                else {
                    self.showAddtoFavAlert(message: "Add audio to your favourite list.", audio: audio, indexPath: indexPath)
                }
            }
        }
    }
}

//MARK:- PlayAudioViewModel Delegates Implementation
extension SearchViewController: PlayAudioViewModelDelegates {
    func audioAddedtoFav(indexPath: IndexPath) {
        showToastMessage(withMessage: "Audio added.")
        if let cell = self.tableView.cellForRow(at: indexPath) as? SearchTableViewCell {
            if self.tableView.visibleCells.contains(cell) {
                DispatchQueue.main.async {
                    self.cellAnimation = false
                    self.tableView.reloadRows(at: [indexPath], with: .fade)
                    DispatchQueue.global().asyncAfter(deadline: .now() + 1.5, execute: { self.cellAnimation = true })
                }
            }
        }
    }
    
    func categoryImageFetched(image: String) {
    }
    
    func audiosFetched(audios: [AudioObjectModel]) {
    }
    
    func onFailiure(_ errorMessage: String) {
        showErrorMessage(withMessage: errorMessage)
    }
}
