//
//  SearchViewModel.swift
//  Motivational Audios
//
//  Created by Irfan Ahmed on 25/08/2020.
//  Copyright © 2020 Farhan. All rights reserved.
//

import Foundation
import FirebaseDatabase

protocol SearchViewModelDelegate {
    func onSuccess(searchedAudio: [AudioObjectModel])
    func categoriesFetched(categories: [CategoryObjectModel])
    func onSearchFailiure(_ errorMessage: String)
}

class SearchViewModel {
    
    //MARK:- Properties
    var delegate: SearchViewModelDelegate?
    var searchedList = [AudioObjectModel]()
    var allCategories = [CategoryObjectModel]()
    let dispatchGroup = DispatchGroup()
    
    func getCategoriesAudios(categoryid: String){
        searchedList.removeAll()
        dispatchGroup.enter()
            databaseRootReference.child("AllAudios").child(categoryid).observe(.value) { (snapshot) in
                if snapshot.exists() {
                    let audios = snapshot.children
                    for audio in audios {
                        let audioChildData = audio as! DataSnapshot
                        let values = audioChildData.value as! [String : Any]
                        let obj = AudioObjectModel()
                        if let audio = values["audio"] as? String { obj.audio = audio }
                        if let title = values["title"] as? String { obj.title = title }
                        if let audioBy = values["audioBy"] as? String { obj.audioBy = audioBy }
                        if let categoryId = values["categoryId"] as? String { obj.categoryId = categoryId }
                        if let audioId = values["audioId"] as? String { obj.audioId = audioId }
                        
                        self.searchedList.append(obj)
                    }
                }
                self.dispatchGroup.leave()
            }
        
        dispatchGroup.notify(queue: .main) {
            guard let delegate = self.delegate else { return }
            delegate.onSuccess(searchedAudio: self.searchedList)
        }
    }

    
    func getCategories(){
        databaseRootReference.child("AllCategories").observe(.value, with: { (snapshot) in
            let categories = snapshot.children
            self.allCategories.removeAll()
            for category in categories {
                let categoryChildData = category as! DataSnapshot
                let values = categoryChildData.value as! [String : Any]
                let obj = CategoryObjectModel()
                if let id = values["id"] as? String { obj.id = id }
                if let title = values["title"] as? String { obj.title = title }
                if let cover = values["cover"] as? String { obj.cover = cover }

                self.allCategories.append(obj)
            }
            
             guard let delegate = self.delegate else { return }
            delegate.categoriesFetched(categories: self.allCategories)
        })
    }

    
}
