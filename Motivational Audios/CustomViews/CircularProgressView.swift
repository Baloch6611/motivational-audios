//
//  CircularProgressView.swift
//  Motivational Audios
//
//  Created by Farhan on 03/02/2021.
//  Copyright © 2021 Farhan. All rights reserved.
//

import Foundation
import UIKit

class CircularProgressView: UIView {
    
    // First create two layer properties
    private var circleLayer = CAShapeLayer()
    private var progressLayer = CAShapeLayer()


    override init(frame: CGRect) {
        super.init(frame: frame)
        createCircularPath()
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        createCircularPath()
    }

    func createCircularPath() {
        let circularPath = UIBezierPath(arcCenter: CGPoint(x: frame.size.width / 2.0, y: frame.size.height / 2.0), radius: 120, startAngle: 3 * .pi / 2, endAngle: -.pi / 2, clockwise: false)
        circleLayer.path = circularPath.cgPath
        circleLayer.fillColor = UIColor.clear.cgColor
        circleLayer.lineCap = .round
        circleLayer.lineWidth = 20.0
        circleLayer.strokeColor = UIColor.lightGray.cgColor
        progressLayer.path = circularPath.cgPath
        progressLayer.fillColor = UIColor.clear.cgColor
        progressLayer.lineCap = .round
        progressLayer.lineWidth = 15.0
        progressLayer.strokeEnd = 0
        progressLayer.strokeColor = Constant.shared.getColor(color: .orange).cgColor
        layer.addSublayer(circleLayer)
        layer.addSublayer(progressLayer)
    }

    func progressAnimation(duration: TimeInterval) {
        let circularProgressAnimation = CABasicAnimation(keyPath: "strokeEnd")
        circularProgressAnimation.duration = duration
        circularProgressAnimation.toValue = 1.0
        circularProgressAnimation.fillMode = .forwards
        circularProgressAnimation.isRemovedOnCompletion = false
        progressLayer.add(circularProgressAnimation, forKey: "progressAnim")
    }
    
//    //MARK: awakeFromNib
//
//    override func awakeFromNib() {
//        super.awakeFromNib()
//        setupView()
//        label.text = "0"
//    }
//
//
//    //MARK: Public
//
//    public var lineWidth:CGFloat = 10 {
//        didSet{
//            foregroundLayer.lineWidth = lineWidth
//            backgroundLayer.lineWidth = lineWidth - (0.20 * lineWidth)
//        }
//    }
//
//    public var labelSize: CGFloat = 60 {
//        didSet {
//            label.font = UIFont(name: Constant.shared.getFontName(font: .semi_bold), size: 50)
//            label.sizeToFit()
//            configLabel()
//        }
//    }
//
//    public var safePercent: Int = 100 {
//        didSet{
//            setForegroundLayerColorForSafePercent()
//        }
//    }
//
//    public func setProgress(to progressConstant: Double, withAnimation: Bool) {
//
//        var progress: Double {
//            get {
//                if progressConstant > 1 { return 1 }
//                else if progressConstant < 0 { return 0 }
//                else { return progressConstant }
//            }
//        }
//
//        foregroundLayer.strokeEnd = CGFloat(progress)
//
//        if withAnimation {
//            let animation = CABasicAnimation(keyPath: "strokeEnd")
//            animation.fromValue = 0
//            animation.toValue = progress
//            animation.duration = 30
//            foregroundLayer.add(animation, forKey: "foregroundAnimation")
//
//        }
//
//        var currentTime:Double = 0
//        let timer = Timer.scheduledTimer(withTimeInterval: 1.0, repeats: true) { (timer) in
//            if currentTime >= 30{
//                timer.invalidate()
//            } else {
//                currentTime += 1.0
//                let percent = currentTime/30 * 100
//                self.label.text = "\(Int(progress * percent))"
//                self.setForegroundLayerColorForSafePercent()
//                self.configLabel()
//            }
//        }
//        timer.fire()
//
//    }
//
//
//
//
//    //MARK: Private
//    private var label = UILabel()
//    private let foregroundLayer = CAShapeLayer()
//    private let backgroundLayer = CAShapeLayer()
//    private var radius: CGFloat {
//        get{
//            if self.frame.width < self.frame.height { return (self.frame.width - lineWidth)/2 }
//            else { return (self.frame.height - lineWidth)/2 }
//        }
//    }
//
//    private var pathCenter: CGPoint{ get{ return self.convert(self.center, from:self.superview) } }
//    private func makeBar(){
//        self.layer.sublayers = nil
//        drawBackgroundLayer()
//        drawForegroundLayer()
//    }
//
//    private func drawBackgroundLayer(){
//        let path = UIBezierPath(arcCenter: pathCenter, radius: self.radius, startAngle: 0, endAngle: 2*CGFloat.pi, clockwise: true)
//        self.backgroundLayer.path = path.cgPath
//        self.backgroundLayer.strokeColor = UIColor.lightGray.cgColor
//        self.backgroundLayer.lineWidth = lineWidth - (lineWidth * 20/100)
//        self.backgroundLayer.fillColor = UIColor.clear.cgColor
//        self.layer.addSublayer(backgroundLayer)
//
//    }
//
//    private func drawForegroundLayer(){
//
//        let startAngle = (-CGFloat.pi/2)
//        let endAngle = 2 * CGFloat.pi + startAngle
//
//        let path = UIBezierPath(arcCenter: pathCenter, radius: self.radius, startAngle: startAngle, endAngle: endAngle, clockwise: true)
//
//        foregroundLayer.lineCap = CAShapeLayerLineCap.round
//        foregroundLayer.path = path.cgPath
//        foregroundLayer.lineWidth = lineWidth
//        foregroundLayer.fillColor = UIColor.clear.cgColor
//        foregroundLayer.strokeColor = UIColor.red.cgColor
//        foregroundLayer.strokeEnd = 0
//
//        self.layer.addSublayer(foregroundLayer)
//
//    }
//
//    private func makeLabel(withText text: String) -> UILabel {
//        let label = UILabel(frame: CGRect(x: 0, y: 0, width: 0, height: 0))
//        label.text = text
//        label.font = UIFont(name: Constant.shared.getFontName(font: .semi_bold), size: 50)
//        label.sizeToFit()
//        label.center = pathCenter
//        return label
//    }
//
//    private func configLabel(){
//        label.sizeToFit()
//        label.center = pathCenter
//    }
//
//    private func setForegroundLayerColorForSafePercent(){
//        self.foregroundLayer.strokeColor = Constant.shared.getColor(color: .orange).cgColor
//    }
//
//    private func setupView() {
//        makeBar()
//        self.addSubview(label)
//    }
//
//
//
//    //Layout Sublayers
//    private var layoutDone = false
//    override func layoutSublayers(of layer: CALayer) {
//        if !layoutDone {
//            let tempText = label.text
//            setupView()
//            label.text = tempText
//            layoutDone = true
//        }
//    }
    
}
