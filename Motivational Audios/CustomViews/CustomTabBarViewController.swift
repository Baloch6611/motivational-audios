//
//  CustomTabBarViewController.swift
//  Motivational Audios
//
//  Created by Farhan on 25/01/2021.
//  Copyright © 2021 Farhan. All rights reserved.
//

import UIKit

class CustomTabBarViewController: UITabBarController {

    //MARK:- View Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupMiddleButton()
    }

    //MARK:- Custom Methods
    func setupMiddleButton() {
        let middleButton = UIButton.init(type: .custom)
        let tabBarHeight = self.tabBar.bounds.height
        let viewHeight   = self.view.bounds.height
        print(tabBarHeight)
        print(viewHeight)
        let centerButtonVerticalPosition = viewHeight - ( 64 + 30 )
        print(centerButtonVerticalPosition)
        middleButton.frame = CGRect.init(x: self.tabBar.center.x - 32, y: centerButtonVerticalPosition, width: 64, height: 64)
        middleButton.backgroundColor = Constant.shared.getColor(color: .orange)
        middleButton.layer.cornerRadius = 32
        let middleButtonImage = Constant.shared.getImage(image: .mic)
        middleButton.setImage(middleButtonImage, for: .normal)
        middleButton.addTarget(self, action: #selector(middleButtonAction(sender:)), for: .touchUpInside)
        self.view.insertSubview(middleButton, aboveSubview: self.tabBar)
        middleButton.addTarget(self, action: #selector(middleButtonAction(sender:)), for: .touchUpInside)

        view.layoutIfNeeded()
    }

    @objc private func middleButtonAction(sender: UIButton) { selectedIndex = 2 }
    
    
}
