//
//  Enums.swift
//  Motivational Audios
//
//  Created by ADMIN on 8/12/20.
//  Copyright © 2020 Farhan. All rights reserved.
//

import Foundation
import UIKit

enum Color {
    case lightGrey, red, orange
}

enum audioRecording: String {
    case start, resume, stop
}

enum finishButtonState: String {
    case finish, play
}


enum UserDefaultsKeys : String {
    case isLoggedIn
    case userID
    case loginWithEmail
}

enum SearchTableViewCellType {
    case search, playlist
}

enum LandingScreen: String {
    case login
    case main
}

enum FontName: String {
    case popins = "Poppins"
    case light = "Poppins-Light"
    case medium = "Poppins-Medium"
    case semi_bold = "Poppins-SemiBold"
    case regular = "Poppins-Regular"
}

enum HomePlaylists: String {
    case recentlyPlayed = "Recently Played"
    case moodsAndCollection = "Moods & Collection"
    case searchList = "Search List"
}

enum PlayAudioSuccess: String {
    case addedToFavourite = "Audio added to your favourite list."
    case addToFavourite
    case alreadyAddedToFavourite
}

enum AudioRepeat: String {
    case repeate = "Repeat all"
    case shuffel = "Shuffel on"
    case loop = "Single repeat"
}

enum Image: String {
    case defaultUser
    case defaultThumbnail
    case soundOn
    case mute
    case pause
    case play
    case back
    case share
    case bell
    case like
    case liked
    case shuffel
    case repeatAudio
    case loop
    case gallery
    case warning
    case logout
    case mic
}

