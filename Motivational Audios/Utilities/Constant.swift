//
//  Constant.swift
//  Motivational Audios
//
//  Created by ADMIN on 8/12/20.
//  Copyright © 2020 Farhan. All rights reserved.
//

import Foundation
import UIKit
import Firebase
import FirebaseDatabase


//MARK:- Typealias
typealias TableViewMethods = UITableViewDelegate & UITableViewDataSource
typealias CollectionViewMethods = UICollectionViewDelegate & UICollectionViewDataSource & UICollectionViewDelegateFlowLayout

//MARK:- Messages
let something_went_wrong = "Something went wrong"
let email_already_exists = "An account already exists with the same email address but different sign-in credentials. Sign in using a provider associated with this email address."

//MARK:- Firebase References
var firebaseURL = "https://motivationalaudios-77713.firebaseio.com/"
let databaseRootReference = Database.database().reference()

class Constant {
    
    static let shared = Constant()
    static let twitterKey = "EvwpiqFIJeSANkPxQgNgINtqi"
    static let twitterSecret = "O0PObV1Banh0ArNH4J0ptGL8nyf8KodBzGBfoFR4SQbkEnjGv2"


    
    func getColor(color: Color) -> UIColor {
        switch color {
        case .lightGrey:
            return #colorLiteral(red: 0.9411764706, green: 0.9411764706, blue: 0.9411764706, alpha: 1)
        case .red:
            return #colorLiteral(red: 0.8588235294, green: 0.05882352941, blue: 0.05882352941, alpha: 1)
        case .orange:
            return #colorLiteral(red: 0.8588235294, green: 0.2705882353, blue: 0.05882352941, alpha: 1)
        }
    }
    
    func getFontName(font: FontName) -> String { return font.rawValue }
    
    func getImage(image: Image) -> UIImage {
        guard let image = UIImage(named: image.rawValue) else { return UIImage() }
        return image
    }
        
}


