//
//  Lottie.swift
//  ReadyWheels_Driver
//
//  Created by Ansar  on 26/08/2020.
//  Copyright © 2020 ADMIN. All rights reserved.
//

import UIKit
import Lottie

class LottieAnimation: UIView {
    
    //MARK:- IBOutlets
    @IBOutlet var parentView: UIView!
    @IBOutlet weak var indicatorView: AnimationView!
    
    //MARK:- Variables
    static let instance = LottieAnimation()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("Lottie", owner: self, options: nil)
        commonInit()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //MARK:- Custom Methods
    private func commonInit() {
        parentView.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
        parentView.autoresizingMask = [.flexibleHeight, .flexibleWidth]
    }
    
    func startLottieAnimation() {
        Bundle.main.loadNibNamed("Lottie", owner: self, options: nil)
        commonInit()
        let lottieAnimationView =  AnimationView(name: "loading")
        indicatorView.contentMode = .scaleAspectFit
        self.indicatorView.addSubview(lottieAnimationView)
        lottieAnimationView.frame = self.indicatorView.bounds
        lottieAnimationView.loopMode = .loop
        lottieAnimationView.play()
        
        UIApplication.shared.keyWindow?.addSubview(parentView)
    }
    
    func stopLottieAnimation(){
        parentView.removeFromSuperview()
    }
    
}

