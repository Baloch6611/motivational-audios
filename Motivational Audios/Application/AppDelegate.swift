//
//  AppDelegate.swift
//  Motivational Audios
//
//  Created by ADMIN on 8/12/20.
//  Copyright © 2020 Farhan. All rights reserved.
//

import UIKit
import IQKeyboardManagerSwift
import SwiftMessageBar
import Firebase
import TwitterKit
import FBSDKCoreKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        sleep(UInt32(3.0))
        FirebaseApp.configure()
        TWTRTwitter.sharedInstance().start(withConsumerKey: Constant.twitterKey, consumerSecret: Constant.twitterSecret)
        ApplicationDelegate.shared.application(application, didFinishLaunchingWithOptions: launchOptions)
        IQKeyboardManager.shared.enable = true
        customizeMessageBar()
        decideLandingScreen()
        
        return true
    }
    
    func application(_ app: UIApplication, open url: URL, options: [UIApplication.OpenURLOptionsKey : Any] = [:]) -> Bool {
      return TWTRTwitter.sharedInstance().application(app, open: url, options: options)
    }
    
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return ApplicationDelegate.shared.application(application, open: url, sourceApplication: sourceApplication, annotation: annotation)
    }
    
    func applicationWillResignActive(_ application: UIApplication) {
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
    }

    func applicationWillTerminate(_ application: UIApplication) {
    }
    
    //MARK:- Custom Methods
    private func customizeMessageBar(){
        let config = SwiftMessageBar.Config.Builder()
            .withSuccessColor(#colorLiteral(red: 0.2980392157, green: 0.8196078431, blue: 0.2156862745, alpha: 1))
            .withErrorColor(#colorLiteral(red: 1, green: 0.2196078431, blue: 0.2196078431, alpha: 1))
            .withInfoColor(#colorLiteral(red: 1, green: 0.8, blue: 0, alpha: 1))
            .withInfoIcon(Constant.shared.getImage(image: .warning))
            .withTitleFont(UIFont(name: Constant.shared.getFontName(font: .semi_bold), size: 20) ?? .boldSystemFont(ofSize: 20))
            .withMessageFont(UIFont(name: Constant.shared.getFontName(font: .medium), size: 17) ?? .boldSystemFont(ofSize: 17))
            .build()
        SwiftMessageBar.setSharedConfig(config)
    }
    
    private func addNavBarBackButtonImage(){
        guard let backButtonImage = UIImage(named: "back") else { return }
        let backImage = backButtonImage.withRenderingMode(.alwaysOriginal)
        UINavigationBar.appearance().backIndicatorImage = backImage
        UINavigationBar.appearance().backIndicatorTransitionMaskImage = backImage
        UIBarButtonItem.appearance().setBackButtonTitlePositionAdjustment(UIOffset(horizontal: 0, vertical: 0), for:UIBarMetrics.default)
    }
    
    private func decideLandingScreen(){
        guard let window = window else { return }
        
        if UserDefaults.standard.isLoggedIn() { window.rootViewController = UIStoryboard.initialViewController(for: .Main) }
        else { window.rootViewController = UIStoryboard.initialViewController(for: .Registration) }

        window.makeKeyAndVisible()
    }

}

