//
//  UISlider+Extension.swift
//  Motivational Audios
//
//  Created by Farhan on 14/09/2020.
//  Copyright © 2020 Farhan. All rights reserved.
//

import Foundation
import UIKit

extension UISlider {
    fileprivate func makeCircleWith(size: CGSize, backgroundColor: UIColor) -> UIImage? {
        UIGraphicsBeginImageContextWithOptions(size, false, 0.0)
        let context = UIGraphicsGetCurrentContext()
        context?.setFillColor(backgroundColor.cgColor)
        context?.setStrokeColor(UIColor.clear.cgColor)
        let bounds = CGRect(origin: .zero, size: size)
        context?.addEllipse(in: bounds)
        context?.drawPath(using: .fill)
        let image = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        return image
    }
    
        func setSliderThumbSize(_ size: CGSize) {
            let circleImage = makeCircleWith(size: size, backgroundColor: .white)
            self.setThumbImage(circleImage, for: .normal)
            self.setThumbImage(circleImage, for: .highlighted)
    }
}
