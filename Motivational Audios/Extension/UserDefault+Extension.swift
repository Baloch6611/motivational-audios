//
//  UserDefault+Extension.swift
//  Motivational Audios
//
//  Created by Farhan on 12/09/2020.
//  Copyright © 2020 Farhan. All rights reserved.
//

import Foundation
import UIKit

extension UserDefaults{

    //MARK: Check Login
    func setLoggedIn(value: Bool) {
        set(value, forKey: UserDefaultsKeys.isLoggedIn.rawValue)
        UserDefaults.standard.synchronize()
    }

    func isLoggedIn() -> Bool { return bool(forKey: UserDefaultsKeys.isLoggedIn.rawValue) }
    
    func loginWithEmail(value: Bool) {
        set(value, forKey: UserDefaultsKeys.loginWithEmail.rawValue)
        UserDefaults.standard.synchronize()
    }
    
    func isUserLoggedInWithEmail() -> Bool { return bool(forKey: UserDefaultsKeys.loginWithEmail.rawValue) }


    //MARK: Save User Data
    func setUserID(value: String){
        set(value, forKey: UserDefaultsKeys.userID.rawValue)
        UserDefaults.standard.synchronize()
    }

    //MARK: Retrieve User Data
    func getUserID() -> String { return value(forKey: UserDefaultsKeys.userID.rawValue) as? String ?? "" }
}
