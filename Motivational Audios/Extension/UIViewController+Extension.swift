//
//  UIViewController.swift
//  FlippingWeight
//
//  Created by ADMIN on 6/6/20.
//  Copyright © 2020 ADMIN. All rights reserved.
//

import UIKit
import SwiftMessageBar

extension UIViewController {
    
    //MARK:- NavBar
    func hideNavBar(){
        self.navigationController?.navigationBar.isHidden = true
    }
    
    func showNavBar(){
        self.navigationController?.navigationBar.isHidden = false
    }
    
    func popVC(){
        DispatchQueue.main.async {
            self.navigationController?.popViewController(animated: true)
        }
    }
    
    func hideNavBarBottomLine(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    
    func setCustomFontNavBarTitle(title: String){
        let attrs = [
            NSAttributedString.Key.foregroundColor: UIColor.white,
            NSAttributedString.Key.font: UIFont(name: Constant.shared.getFontName(font: .semi_bold), size: 20)!
        ]

        UINavigationBar.appearance().titleTextAttributes = attrs
        
        self.navigationItem.title = title
    }
    
    func addNavBarImage() {
        
        let navController = navigationController!
        
        let image = UIImage(named: "lion image white")
        let imageView = UIImageView(image: image)
        
        let bannerWidth = navController.navigationBar.frame.size.width
        let bannerHeight = navController.navigationBar.frame.size.height
        
        let bannerX = bannerWidth / 2 - (image?.size.width)! / 2
        let bannerY = bannerHeight / 2 - (image?.size.height)! / 2
        
        imageView.frame = CGRect(x: bannerX, y: bannerY, width: bannerWidth, height: bannerHeight)
        imageView.contentMode = .scaleAspectFit
        
        navigationItem.titleView = imageView
    }
    
    //MARK:- Message
    func showErrorMessage(withMessage message: String){
        SwiftMessageBar.showMessage(withTitle: "Error!", message: message, type: .error)
    }
    
    func showSucessMessage(withMessage message: String){
        SwiftMessageBar.showMessage(withTitle: "Success!", message: message, type: .success)
    }
    
    func showWarningMessage(withMessage message: String){
        SwiftMessageBar.showMessage(withTitle: "Warning!", message: message, type: .info)
    }

    
    func showToastMessage(withMessage message: String ) {
        let font = UIFont(name: Constant.shared.getFontName(font: .medium), size: 14)
        let toastLabel = UILabel(frame: CGRect(x: self.view.frame.size.width/2 - 75, y: self.view.frame.size.height-200, width: 150, height: 35))
        toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
        toastLabel.textColor = UIColor.white
        toastLabel.font = font
        toastLabel.textAlignment = .center;
        toastLabel.text = message
        toastLabel.alpha = 1.0
        toastLabel.layer.cornerRadius = 10;
        toastLabel.clipsToBounds  =  true
        self.view.addSubview(toastLabel)
        UIView.animate(withDuration: 4.0, delay: 0.1, options: .curveEaseOut, animations: {
             toastLabel.alpha = 0.0
        }, completion: {(isCompleted) in
            toastLabel.removeFromSuperview()
        })
    }
        
    //MARK:- Activity Indicator
    func startLoading(){ LottieAnimation.instance.startLottieAnimation() }
    
    func stopLoadingAnimation(){ LottieAnimation.instance.stopLottieAnimation() }
    
    func createFooterSpinner() -> UIView  {
        let footerView = UIView(frame: CGRect(x: 0, y: 0, width: view.frame.size.width, height: 100))
        let spinner = UIActivityIndicatorView()
        spinner.center = footerView.center
        spinner.color = .darkGray
        footerView.addSubview(spinner)
        spinner.startAnimating()
        
        return footerView
    }
    
    //MARK:- UI Customization
    func setGradientBackground(view: UIView) {
        let colorTop =  Constant.shared.getColor(color: .red).cgColor
        let colorBottom = Constant.shared.getColor(color: .orange).cgColor
        
        let gradientLayer = CAGradientLayer()
        gradientLayer.colors = [colorTop, colorBottom]
        gradientLayer.locations = [0.0, 1.0]
        gradientLayer.frame = view.bounds
        
        view.layer.insertSublayer(gradientLayer, at:0)
    }
    
    
    func showAlert(on: UIViewController, style: UIAlertController.Style, title: String?, message: String?, actions: [UIAlertAction] = [UIAlertAction(title: "Ok", style: .default, handler: nil)], completion: (() -> Swift.Void)? = nil) {
      let alert = UIAlertController(title: title, message: message, preferredStyle: style)
      for action in actions {
        alert.addAction(action)
      }
      on.present(alert, animated: true, completion: completion)
    }
    
    //MARK:- Animation
    
}

