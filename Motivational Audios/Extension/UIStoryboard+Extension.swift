//
//  UIStoryboard+Extension.swift
//  FlippingWeight
//
//  Created by ADMIN on 6/6/20.
//  Copyright © 2020 ADMIN. All rights reserved.
//

import UIKit

extension UIStoryboard {
    enum SBName : String {
        case Registration, Main, Details
        var fileName : String {
            return rawValue
        }
        
    }
    
    convenience init(type: SBName, bundle: Bundle? = nil) {
        self.init(name: type.fileName, bundle: bundle)
    }
    
    static func initialViewController(for type: SBName) -> UIViewController {
        let storyboard = UIStoryboard(type: type)
        guard let initialViewController = storyboard.instantiateInitialViewController() else {
            fatalError("Couldn't instantiate initial view controller for \(type.fileName) storyboard.")
        }
        
        return initialViewController
        
    }
    
    static func instantiateViewController(for type: SBName, indentifier: String) -> UIViewController {
        let storyboard = UIStoryboard(type: type)
        let instantiatedViewController = storyboard.instantiateViewController(withIdentifier: indentifier)
        return instantiatedViewController
        
    }
    
}
