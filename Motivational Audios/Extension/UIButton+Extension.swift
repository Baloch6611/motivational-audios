//
//  UIButton+Extension.swift
//  Motivational Audios
//
//  Created by Irfan Ahmed on 27/08/2020.
//  Copyright © 2020 Farhan. All rights reserved.
//

import Foundation
import UIKit

extension UIButton {
    func setImageWithTintColor(image: UIImage, color: UIColor){
        self.setImage(image.withRenderingMode(.alwaysTemplate), for: .normal)
        self.tintColor = .white
    }
    
    func setButtonImage(image: UIImage, animationOption: UIView.AnimationOptions) {
        UIView.animate(withDuration: 1.0) {
            UIView.transition(with: self, duration: 1.0, options: animationOption, animations: {
                self.setImage(image, for: .normal)
            }, completion: nil)
        }
    }

}


extension UIBarButtonItem {

    static func menuButton(_ target: Any?, action: Selector, image: UIImage) -> UIBarButtonItem {
        let button = UIButton(type: .system)
        button.setImage(image, for: .normal)
        button.addTarget(target, action: action, for: .touchUpInside)

        let menuBarItem = UIBarButtonItem(customView: button)
        menuBarItem.customView?.translatesAutoresizingMaskIntoConstraints = false
        menuBarItem.customView?.heightAnchor.constraint(equalToConstant: 24).isActive = true
        menuBarItem.customView?.widthAnchor.constraint(equalToConstant: 24).isActive = true

        return menuBarItem
    }
}
