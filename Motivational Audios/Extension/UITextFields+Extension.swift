//
//  UITextFields+Extension.swift
//  Motivational Audios
//
//  Created by ADMIN on 8/12/20.
//  Copyright © 2020 Farhan. All rights reserved.
//

import Foundation
import UIKit

extension UITextField {
    
    func addShadow(cornerRadius: CGFloat) {
        self.backgroundColor = UIColor.white
        self.layer.masksToBounds = false
        self.layer.shadowColor = UIColor.black.cgColor
        self.layer.shadowOffset = CGSize(width: 0, height: 0)
        self.layer.shadowOpacity = 0.2
        self.backgroundColor = .white
        self.layer.cornerRadius = cornerRadius
    }
}
